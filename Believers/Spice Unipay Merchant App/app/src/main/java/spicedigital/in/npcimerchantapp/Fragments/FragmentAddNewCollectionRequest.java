package spicedigital.in.npcimerchantapp.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.support_classes.Contact;

/**
 * Created by CH-E01039 on 3/19/2016.
 */
public class FragmentAddNewCollectionRequest extends DialogFragment implements View.OnClickListener {


    Context context;
    View rootView;
    EditText editTextAmount, editTextEventName, editTextDescription;
    public static Button btnSelectMembers;
    Button btnCancel, btnSave;
    TextView textOneTime, textMonthly, textYearly, editTextDate;
    public static ArrayList<String> addMembers;
    public static ArrayList<Contact> contactArrayList;
    CheckBox proceed_with_sms;

    private BroadcastReceiver bReceiver;

    public FragmentAddNewCollectionRequest() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_new_collection_request_dailog, container,
                false);
        context = getActivity();
        initilizeView(rootView);

        addMember();

        return rootView;
    }

    private void initilizeView(View mrootView) {
        try {
            addMembers = new ArrayList<String>();
            contactArrayList = new ArrayList<Contact>();
            addMembers = new ArrayList<String>();
            editTextAmount = (EditText) mrootView.findViewById(R.id.editTextAmount);
            editTextEventName = (EditText) mrootView.findViewById(R.id.editTextEventName);
            editTextDate = (TextView) mrootView.findViewById(R.id.editTextDate);
            editTextDescription = (EditText) mrootView.findViewById(R.id.editTextDescription);
            btnSelectMembers = (Button) mrootView.findViewById(R.id.btnSelectMembers);
            btnCancel = (Button) mrootView.findViewById(R.id.btnCancel);
            btnSave = (Button) mrootView.findViewById(R.id.btnSave);
            textOneTime = (TextView) mrootView.findViewById(R.id.textOneTime);
            textMonthly = (TextView) mrootView.findViewById(R.id.textMonthly);
            textYearly = (TextView) mrootView.findViewById(R.id.textYearly);
            proceed_with_sms = (CheckBox) mrootView.findViewById(R.id.proceed_with_sms);


            btnSave.setOnClickListener(this);
            btnCancel.setOnClickListener(this);
            btnSelectMembers.setOnClickListener(this);
            editTextDate.setOnClickListener(this);

            textOneTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textOneTime.setBackgroundResource(R.color.colorSkyblue);
                    textMonthly.setBackgroundResource(R.drawable.button_border);
                    textYearly.setBackgroundResource(R.drawable.button_border);
                    textOneTime.setTextColor(getResources().getColor(R.color.white));
                    textMonthly.setTextColor(getResources().getColor(R.color.colorText));
                    textYearly.setTextColor(getResources().getColor(R.color.colorText));
                }
            });
            textMonthly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textOneTime.setBackgroundResource(R.drawable.button_border);
                    textMonthly.setBackgroundResource(R.color.colorSkyblue);
                    textYearly.setBackgroundResource(R.drawable.button_border);
                    textOneTime.setTextColor(getResources().getColor(R.color.colorText));
                    textMonthly.setTextColor(getResources().getColor(R.color.colorText));
                    textYearly.setTextColor(getResources().getColor(R.color.colorText));
                }
            });
            textYearly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textOneTime.setBackgroundResource(R.drawable.button_border);
                    textMonthly.setBackgroundResource(R.drawable.button_border);
                    textYearly.setBackgroundResource(R.color.colorSkyblue);
                    textOneTime.setTextColor(getResources().getColor(R.color.colorText));
                    textMonthly.setTextColor(getResources().getColor(R.color.colorText));
                    textYearly.setTextColor(getResources().getColor(R.color.white));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        try {
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
        }
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            // safety check
            if (getDialog() == null) {
                return;
            }

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            getDialog().getWindow().setLayout((int) (width * .98f), ViewGroup.LayoutParams.WRAP_CONTENT);


        } catch (Exception e) {
        }
    }


    @Override
    public void onClick(View v) {

        if (v == btnSave) {
            if (editTextAmount.getText().length() < 1) {
                Toast.makeText(context, "Please enter amount", Toast.LENGTH_SHORT).show();
            } else if (Integer.parseInt(editTextAmount.getText().toString()) == 0) {
                Toast.makeText(context, "Please enter valid amount", Toast.LENGTH_SHORT).show();
            } else if (editTextEventName.getText().length() < 1) {
                Toast.makeText(context, "Please enter event name", Toast.LENGTH_SHORT).show();
            } else if (editTextDate.getText().length() < 1) {
                Toast.makeText(context, "Please enter collected date", Toast.LENGTH_SHORT).show();
            } else if (editTextDescription.getText().length() < 1) {
                Toast.makeText(context, "Please enter description", Toast.LENGTH_SHORT).show();
            } else if (addMembers.size() == 0) {
                Toast.makeText(context, "Please select members", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    new SendSMSTask().execute();
                    sendCollectRequest();
                    Log.d("start task", "---------------------------");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        if (v == btnCancel) {
            dismiss();
        }


        if (v == btnSelectMembers) {
            addMembers = new ArrayList<String>();


            setContactArray(contactArrayList);
            EventAddMembers eventAddMembers = new EventAddMembers();
            eventAddMembers.show(getFragmentManager(), "");

            try {
                if (contactArrayList.size() == 0) {
                    try {
                        Toast.makeText(getActivity(), "No members in your list", Toast.LENGTH_LONG).show();
                    } catch (Exception ex) {

                    }
                }
            } catch (Exception e) {

            }

        }
        if (v == editTextDate) {
            DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    myCalendar.set(Calendar.YEAR, year);

                    updatedate(year, monthOfYear, dayOfMonth);


                }
            };

            Calendar myCalendar = Calendar.getInstance();

            new DatePickerDialog(context, d,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }

    }

    protected void updatedate(int yr, int mon, int day) {
        try {


            Date toDate, fromDate = null;


            String dtStart = yr + "/" + (mon + 1) + "/" + day;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            try {
                fromDate = format.parse(dtStart);

                dtStart = format.format(fromDate);

            } catch (Exception e) {
                // TODO Auto-generated catch block

            }


            editTextDate.setText(dtStart);


        } catch (Exception e) {

        }
    }

    public void sendCollectRequest() {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            AppControler.getDataBaseInstance().addAdminCollectionRequest("", "", editTextEventName.getText().toString().trim(),
                    "", editTextDate.getText().toString().trim(), "", "", sharedPreferences.getString("name", ""), sharedPreferences.getString("communityVPA", ""),
                    editTextAmount.getText().toString().trim(), editTextDescription.getText().toString().trim(),
                    editTextDate.getText().toString().trim(), "Pending",
                    "100", "300", sharedPreferences.getString("name", ""), "", "");

            HashMap parameters = new HashMap();
            //  parameters.put("mobilenumber", "7529841084,9888762945");
            String arryofnum = "";
            ArrayList<String> numberList = getMemberList();

            for (int i = 0; i < numberList.size(); i++) {
               /* if (i == getMemberList().size() - 1) {
                    arryofnum = numberList.get(i);
                } else {*/
                arryofnum = arryofnum + numberList.get(i) + ",";
                // }
            }

            parameters.put("aryOfNumbers", arryofnum);
            parameters.put("payerName", "");
            parameters.put("payerAddress", "");
            parameters.put("payeeName", sharedPreferences.getString("name", ""));
            parameters.put("payeeAddress", sharedPreferences.getString("communityVPA", ""));
            parameters.put("eventType", "");
            parameters.put("eventName", editTextEventName.getText().toString().trim());
            parameters.put("amount", editTextAmount.getText().toString().trim());
            parameters.put("startDate", editTextDate.getText().toString());
            parameters.put("endDate", "");
            parameters.put("timeStamp", "");
            parameters.put("eventDesc", editTextDescription.getText().toString().trim());
            parameters.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
            parameters.put("walletId", System.currentTimeMillis());

            parameters.put("ownName", sharedPreferences.getString("name", ""));


            ParseCloud.callFunctionInBackground("push_notification_collect", parameters, new FunctionCallback<Object>() {
                public void done(Object response, ParseException e) {
                    /*if (e == null) {
                    } else {
                        Toast.makeText(context, "Failed, Please try again later", Toast.LENGTH_LONG).show();

                    }*/
                    dismiss();
                    Toast.makeText(context, "Notification has been sent successfully", Toast.LENGTH_LONG).show();


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addMember() {
        contactArrayList = new ArrayList<Contact>();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Member");
        queryUserAccount.whereEqualTo("adminMobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));


        queryUserAccount.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
                if (e == null) {

                    try {
                        for (ParseObject parseObj : parseObjects) {
                            String mobileNumber = parseObj.getString("mobileNumber");
                            String memberName = parseObj.getString("memberName");
                            Contact contact = new Contact();
                            contact.setName(memberName);
                            contact.setNumber(mobileNumber.replace("+91", ""));

                            contactArrayList.add(contact);


                        }
                        /*if(parseObjects.size()==0){
                            try {
                                Toast.makeText(getActivity(), "No members in your list", Toast.LENGTH_LONG).show();
                            }
                            catch (Exception ex){

                            }
                        }*/
                    } catch (Exception bx) {

                    }
                } else {
                    try {
                        Toast.makeText(getActivity(), "No members in your list", Toast.LENGTH_LONG).show();
                    } catch (Exception ex) {

                    }
                }

            }
        });
    }


    public void setContactArray(ArrayList<Contact> contactArrayList) {
        this.contactArrayList = contactArrayList;
    }

    public static ArrayList<Contact> getContactArray() {
        return contactArrayList;
    }

    public static void setAddMembers(String addMemberMb) {
        addMembers.add(addMemberMb);
        btnSelectMembers.setText("ADD " + addMembers.size() + " MEMBERS");
    }

    public ArrayList<String> getMemberList() {
        return addMembers;
    }


    private class SendSMSTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            Log.d("SMS", "in doINBackground");
            sendSMS(getMemberList());
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("sms", "onpreexecute");
        }

    }

    private void sendSMS(ArrayList<String> phoneNumberList) {
        Log.d("SMS", "in send SMS--------" + phoneNumberList.size());
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

        String personName = "";
        if (sharedPreferences.getString("PersonType", "").equalsIgnoreCase("ADMIN")) {
            personName = "Mr. " + sharedPreferences.getString("name", "");
        } else {
            personName = "Mr. " + sharedPreferences.getString("membername", "");
        }


        String SENT = "SMS_SENT";

        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
                new Intent(SENT), 0);

        for (int i = 0; i < phoneNumberList.size(); i++) {
            String message = personName + " has requested Rs. " + editTextAmount.getText().toString().trim() + " for an event - " + editTextEventName.getText().toString().trim();
            Log.d("SMS", message);
            android.telephony.SmsManager sms = android.telephony.SmsManager.getDefault();
            sms.sendTextMessage(phoneNumberList.get(i), null, message, sentPI, null);
        }


    }
}
