package spicedigital.in.npcimerchantapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class MyDataBase extends SQLiteOpenHelper {

    private static final String KEY_ID = "id";
    private static final int DATABASE_VERSION = 1;
    // notification table name
    public static final String TABLE_NOTIFICATIONS = "notifications";
    public static final String TABLE_ADMINCOLLECT = "admincollect";
    public static final String TABLE_PAYMEMBERAPPROVAL = "paymemberrequest";
    // notification Table Columns names
    public static final String KEY_NOTIFICATION_ID = "notificationid";
    public static final String KEY_COMMUNITY_ID = "communityid";
    public static final String KEY_EVENT_NAME = "eventname";
    public static final String KEY_EVENT_TYPE = "eventtype";
    public static final String KEY_EVENT_TIMESTAMP = "eventtimestamp";
    public static final String KEY_PAYER_NAME = "payername";
    public static final String KEY_PAYER_VPA = "payervpa";
    public static final String KEY_MERCHANT_NAME = "merchant_name";
    public static final String KEY_MERCHANT_VPA = "merchant_vpa";
    public static final String KEY_AMOUNT = "amount";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_CURRENT_DATE = "current_date";
    public static final String KEY_CURRENT_STATUS = "current_status";

    public static final String KEY_UDF1 = "udf1";
    public static final String KEY_UDF2 = "udf2";
    public static final String KEY_UDF3 = "udf3";
    public static final String KEY_UDF4 = "udf4";
    public static final String KEY_UDF5 = "udf5";


    private static final String CREATE_TABLE_NOTIFICATIONS = "CREATE TABLE "
            + TABLE_NOTIFICATIONS + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NOTIFICATION_ID + " TEXT," + KEY_COMMUNITY_ID + " TEXT,"
            + KEY_EVENT_NAME + " TEXT,"
            + KEY_EVENT_TYPE + " TEXT,"
            + KEY_EVENT_TIMESTAMP + " TEXT," + KEY_PAYER_NAME + " TEXT," + KEY_PAYER_VPA + " TEXT,"
            + KEY_MERCHANT_NAME + " TEXT," + KEY_MERCHANT_VPA + " TEXT," + KEY_AMOUNT + " TEXT," + KEY_CURRENT_STATUS + " TEXT," + KEY_UDF1 + " TEXT," + KEY_UDF2 + " TEXT," + KEY_UDF3 + " TEXT," + KEY_UDF4 + " TEXT," + KEY_UDF5 + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_CURRENT_DATE + " TEXT );";

    private static final String CREATE_ADMINCOLLECT = "CREATE TABLE "
            + TABLE_ADMINCOLLECT + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NOTIFICATION_ID + " TEXT," + KEY_COMMUNITY_ID + " TEXT,"
            + KEY_EVENT_NAME + " TEXT,"
            + KEY_EVENT_TYPE + " TEXT,"
            + KEY_EVENT_TIMESTAMP + " TEXT," + KEY_PAYER_NAME + " TEXT," + KEY_PAYER_VPA + " TEXT,"
            + KEY_MERCHANT_NAME + " TEXT," + KEY_MERCHANT_VPA + " TEXT," + KEY_AMOUNT + " TEXT," + KEY_CURRENT_STATUS + " TEXT," + KEY_UDF1 + " TEXT," + KEY_UDF2 + " TEXT," + KEY_UDF3 + " TEXT," + KEY_UDF4 + " TEXT," + KEY_UDF5 + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_CURRENT_DATE + " TEXT );";

    private static final String CREATE_PAYMEMBERAPPROVAL = "CREATE TABLE "
            + TABLE_PAYMEMBERAPPROVAL + "(" + KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NOTIFICATION_ID + " TEXT," + KEY_COMMUNITY_ID + " TEXT,"
            + KEY_EVENT_NAME + " TEXT,"
            + KEY_EVENT_TYPE + " TEXT,"
            + KEY_EVENT_TIMESTAMP + " TEXT," + KEY_PAYER_NAME + " TEXT," + KEY_PAYER_VPA + " TEXT,"
            + KEY_MERCHANT_NAME + " TEXT," + KEY_MERCHANT_VPA + " TEXT," + KEY_AMOUNT + " TEXT," + KEY_CURRENT_STATUS + " TEXT," + KEY_UDF1 + " TEXT," + KEY_UDF2 + " TEXT," + KEY_UDF3 + " TEXT," + KEY_UDF4 + " TEXT," + KEY_UDF5 + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_CURRENT_DATE + " TEXT );";


    private static String DB_NAME = "notification_database";
    private Context mContext;


    public MyDataBase(Context context) throws IOException {
        super(context, DB_NAME, null, DATABASE_VERSION);
        try {

            this.mContext = context;


        } catch (Exception e) {

        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(CREATE_TABLE_NOTIFICATIONS);
        sqLiteDatabase.execSQL(CREATE_ADMINCOLLECT);
        sqLiteDatabase.execSQL(CREATE_PAYMEMBERAPPROVAL);



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public long addNotification(String notificationID, String communityID, String eventName, String eventType,
                                String timeStmap, String payerName, String payerVPA,
                                String merchantname, String merchantVpa, String amount,
                                String description, String currentDate, String currentStatus,
                                String udf1, String udf2, String udf3, String udf4, String udf5) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_NOTIFICATION_ID, notificationID);
        values.put(KEY_COMMUNITY_ID, communityID);
        values.put(KEY_EVENT_NAME, eventName);
        values.put(KEY_EVENT_TYPE, eventType);
        values.put(KEY_EVENT_TIMESTAMP, timeStmap);
        values.put(KEY_PAYER_NAME, payerName);
        values.put(KEY_PAYER_VPA, payerVPA);
        values.put(KEY_MERCHANT_NAME, merchantname);
        values.put(KEY_MERCHANT_VPA, merchantVpa);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_CURRENT_DATE, currentDate);
        values.put(KEY_CURRENT_STATUS, currentStatus);
        values.put(KEY_UDF1, udf1);
        values.put(KEY_UDF2, udf2);
        values.put(KEY_UDF3, udf3);
        values.put(KEY_UDF4, udf4);
        values.put(KEY_UDF5, udf5);

        // insert row in notification table

        long insert = db.insert(TABLE_NOTIFICATIONS, null, values);
        return insert;
    }

    public long addMemberApprovalPayRequest(String notificationID, String communityID, String eventName, String eventType,
                                String timeStmap, String payerName, String payerVPA,
                                String merchantname, String merchantVpa, String amount,
                                String description, String currentDate, String currentStatus,
                                String udf1, String udf2, String udf3, String udf4, String udf5) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_NOTIFICATION_ID, notificationID);
        values.put(KEY_COMMUNITY_ID, communityID);
        values.put(KEY_EVENT_NAME, eventName);
        values.put(KEY_EVENT_TYPE, eventType);
        values.put(KEY_EVENT_TIMESTAMP, timeStmap);
        values.put(KEY_PAYER_NAME, payerName);
        values.put(KEY_PAYER_VPA, payerVPA);
        values.put(KEY_MERCHANT_NAME, merchantname);
        values.put(KEY_MERCHANT_VPA, merchantVpa);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_CURRENT_DATE, currentDate);
        values.put(KEY_CURRENT_STATUS, currentStatus);
        values.put(KEY_UDF1, udf1);
        values.put(KEY_UDF2, udf2);
        values.put(KEY_UDF3, udf3);
        values.put(KEY_UDF4, udf4);
        values.put(KEY_UDF5, udf5);

        // insert row in notification table

        long insert = db.insert(TABLE_PAYMEMBERAPPROVAL, null, values);
        return insert;
    }


    public long addAdminCollectionRequest(String notificationID, String communityID, String eventName, String eventType,
                                String timeStmap, String payerName, String payerVPA,
                                String merchantname, String merchantVpa, String amount,
                                String description, String currentDate, String currentStatus,
                                String udf1, String udf2, String udf3, String udf4, String udf5) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();


        values.put(KEY_NOTIFICATION_ID, notificationID);
        values.put(KEY_COMMUNITY_ID, communityID);
        values.put(KEY_EVENT_NAME, eventName);
        values.put(KEY_EVENT_TYPE, eventType);
        values.put(KEY_EVENT_TIMESTAMP, timeStmap);
        values.put(KEY_PAYER_NAME, payerName);
        values.put(KEY_PAYER_VPA, payerVPA);
        values.put(KEY_MERCHANT_NAME, merchantname);
        values.put(KEY_MERCHANT_VPA, merchantVpa);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_CURRENT_DATE, currentDate);
        values.put(KEY_CURRENT_STATUS, currentStatus);
        values.put(KEY_UDF1, udf1);
        values.put(KEY_UDF2, udf2);
        values.put(KEY_UDF3, udf3);
        values.put(KEY_UDF4, udf4);
        values.put(KEY_UDF5, udf5);

        // insert row in notification table

        long insert = db.insert(TABLE_ADMINCOLLECT, null, values);
        return insert;
    }

    public ArrayList<HashMap<String, String>> getNotificationList() {
        ArrayList<HashMap<String, String>> mdatalist = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map = new HashMap<>();

        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATIONS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                map = new HashMap<>();


               /* map.put(KEY_NOTIFICATION_ID, c.getString(c
                        .getColumnIndex(KEY_NOTIFICATION_ID)));*/

                map.put(KEY_NOTIFICATION_ID, String.valueOf(c.getString(c.getColumnIndex(KEY_NOTIFICATION_ID))));

                map.put(KEY_COMMUNITY_ID, String.valueOf(c.getString(c.getColumnIndex(KEY_COMMUNITY_ID))));

                map.put(KEY_EVENT_NAME, c.getString(c
                        .getColumnIndex(KEY_EVENT_NAME)));
                map.put(KEY_DESCRIPTION, String.valueOf(c.getString(c.getColumnIndex(KEY_DESCRIPTION))));
                map.put(KEY_AMOUNT, c.getString(c
                        .getColumnIndex(KEY_AMOUNT)));
                map.put(KEY_EVENT_TYPE, c.getString(c
                        .getColumnIndex(KEY_EVENT_TYPE)));
                map.put(KEY_CURRENT_DATE, String.valueOf(c.getString(c.getColumnIndex(KEY_CURRENT_DATE))));

                map.put(KEY_EVENT_TIMESTAMP, c.getString(c
                        .getColumnIndex(KEY_EVENT_TIMESTAMP)));
                map.put(KEY_CURRENT_STATUS, String.valueOf(c.getString(c.getColumnIndex(KEY_CURRENT_STATUS))));

                map.put(KEY_PAYER_NAME, c.getString(c
                        .getColumnIndex(KEY_PAYER_NAME)));
                map.put(KEY_UDF1, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF1))));

                map.put(KEY_PAYER_VPA, c.getString(c
                        .getColumnIndex(KEY_PAYER_VPA)));
                map.put(KEY_UDF2, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF2))));

                map.put(KEY_MERCHANT_NAME, c.getString(c
                        .getColumnIndex(KEY_MERCHANT_NAME)));
                map.put(KEY_UDF3, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF3))));

                map.put(KEY_MERCHANT_VPA, c.getString(c
                        .getColumnIndex(KEY_MERCHANT_VPA)));
                map.put(KEY_UDF4, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF4))));

                map.put(KEY_UDF5, c.getString(c
                        .getColumnIndex(KEY_UDF5)));
                map.put(KEY_ID, String.valueOf(c.getInt(c.getColumnIndex(KEY_ID))));


                mdatalist.add(map);

            } while (c.moveToNext());
        }
        return mdatalist;

    }

    public ArrayList<HashMap<String, String>> getDatafromRequests(String TABLE_NAME) {
        ArrayList<HashMap<String, String>> mdatalist = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map = new HashMap<>();


        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {

                map = new HashMap<>();


               /* map.put(KEY_NOTIFICATION_ID, c.getString(c
                        .getColumnIndex(KEY_NOTIFICATION_ID)));*/
                map.put(KEY_NOTIFICATION_ID, String.valueOf(c.getString(c.getColumnIndex(KEY_NOTIFICATION_ID))));

                map.put(KEY_COMMUNITY_ID, String.valueOf(c.getString(c.getColumnIndex(KEY_COMMUNITY_ID))));

                map.put(KEY_EVENT_NAME, c.getString(c
                        .getColumnIndex(KEY_EVENT_NAME)));
                map.put(KEY_DESCRIPTION, String.valueOf(c.getString(c.getColumnIndex(KEY_DESCRIPTION))));

                map.put(KEY_EVENT_TYPE, c.getString(c
                        .getColumnIndex(KEY_EVENT_TYPE)));
                map.put(KEY_CURRENT_DATE, String.valueOf(c.getString(c.getColumnIndex(KEY_CURRENT_DATE))));

                map.put(KEY_AMOUNT, c.getString(c
                        .getColumnIndex(KEY_AMOUNT)));
                map.put(KEY_EVENT_TIMESTAMP, c.getString(c
                        .getColumnIndex(KEY_EVENT_TIMESTAMP)));
                map.put(KEY_CURRENT_STATUS, String.valueOf(c.getString(c.getColumnIndex(KEY_CURRENT_STATUS))));

                map.put(KEY_PAYER_NAME, c.getString(c
                        .getColumnIndex(KEY_PAYER_NAME)));
                map.put(KEY_UDF1, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF1))));

                map.put(KEY_PAYER_VPA, c.getString(c
                        .getColumnIndex(KEY_PAYER_VPA)));
                map.put(KEY_UDF2, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF2))));

                map.put(KEY_MERCHANT_NAME, c.getString(c
                        .getColumnIndex(KEY_MERCHANT_NAME)));
                map.put(KEY_UDF3, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF3))));

                map.put(KEY_MERCHANT_VPA, c.getString(c
                        .getColumnIndex(KEY_MERCHANT_VPA)));
                map.put(KEY_UDF4, String.valueOf(c.getString(c.getColumnIndex(KEY_UDF4))));

                map.put(KEY_UDF5, c.getString(c
                        .getColumnIndex(KEY_UDF5)));
                map.put(KEY_ID, String.valueOf(c.getInt(c.getColumnIndex(KEY_ID))));


                mdatalist.add(map);

            } while (c.moveToNext());
        }
        return mdatalist;

    }

}
