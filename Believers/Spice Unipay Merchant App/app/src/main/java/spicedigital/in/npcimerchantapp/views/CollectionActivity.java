package spicedigital.in.npcimerchantapp.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.Fragments.FragmentAddNewCollectionRequest;
import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.CollectionRequestAdapter;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.database.MyDataBase;
import spicedigital.in.npcimerchantapp.home_screen.CollectionEventDetail;

/**
 * Created by CH-E01062 on 18-03-2016.
 */
public class CollectionActivity extends AppCompatActivity implements View.OnClickListener {

    ListView mListView;
    CollectionRequestAdapter adapter;
    ArrayList mDataList;
    Toolbar toolbar;
    View view;
    TextView titleText;
    ImageView fabBtn;
    String PersonType = "";
    ArrayList<HashMap<String, String>> getpayrequestformembers;
    ArrayList<HashMap<String, String>> getcollectrequestforAdmins;

    public void initialiseToolbar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.tool_bar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            view = toolbar.getRootView();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collections_view);
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        PersonType = sharedPreferences.getString("PersonType", "");


        initialiseToolbar();
        initialiseviews();



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void initialiseviews() {

        try {
            fabBtn = (ImageView) findViewById(R.id.fab_btn);
            fabBtn.setOnClickListener(this);
            titleText = (TextView) view.findViewById(R.id.title_text);

            if (PersonType.equalsIgnoreCase("ADMIN")) {
                titleText.setText("COLLECTION REQUESTS");
                fabBtn.setVisibility(View.VISIBLE);
            } else {
                titleText.setText("PAYMENT REQUESTS");
                fabBtn.setVisibility(View.GONE);
            }
            mDataList = new ArrayList();
            mDataList.add("1");
            mDataList.add("1");
            mDataList.add("1");
            mDataList.add("1");
            mListView = (ListView) findViewById(R.id.collection_list);

           /* if (PersonType.equalsIgnoreCase("ADMIN")) {
                getcollectrequestforAdmins = AppControler.getDataBaseInstance().getDatafromRequests(MyDataBase.TABLE_ADMINCOLLECT);
                if (getcollectrequestforAdmins != null && getcollectrequestforAdmins.size() > 0) {

                    adapter = new CollectionRequestAdapter(this, getcollectrequestforAdmins);
                    mListView.setAdapter(adapter);

                }
            } else {
                getpayrequestformembers = AppControler.getDataBaseInstance().getDatafromRequests(MyDataBase.TABLE_PAYMEMBERAPPROVAL);
                if (getpayrequestformembers != null && getpayrequestformembers.size() > 0) {

                    adapter = new CollectionRequestAdapter(this, getpayrequestformembers);
                    mListView.setAdapter(adapter);

                }
            }*/

            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(new Intent(CollectionActivity.this, CollectionEventDetail.class
                    ));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            if (PersonType.equalsIgnoreCase("ADMIN")) {
                getcollectrequestforAdmins = new ArrayList<HashMap<String, String>>();
                getcollectrequestforAdmins = AppControler.getDataBaseInstance().getDatafromRequests(MyDataBase.TABLE_ADMINCOLLECT);
                if (getcollectrequestforAdmins != null && getcollectrequestforAdmins.size() > 0) {

                    adapter = new CollectionRequestAdapter(this, getcollectrequestforAdmins);
                    mListView.setAdapter(adapter);

                }
            } else {

            }
        }catch(Exception e){

        }
    }

    @Override
    public void onClick(View v) {
        if (v == fabBtn) {

            if (PersonType.equalsIgnoreCase("ADMIN")) {
                FragmentAddNewCollectionRequest request = new FragmentAddNewCollectionRequest();
                request.show(getSupportFragmentManager(), "");
            } else {

            }


        }
    }
}
