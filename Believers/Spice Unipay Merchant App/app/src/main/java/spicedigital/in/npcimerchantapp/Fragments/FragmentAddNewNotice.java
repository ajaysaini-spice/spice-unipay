package spicedigital.in.npcimerchantapp.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by CH-E01039 on 3/19/2016.
 */
public class FragmentAddNewNotice extends DialogFragment {


    Context context;
    View rootView;
    EditText editTextNoticeName, editTextDescription;
    TextView textDateTo, textDateFrom, textTimeTo, textTimeFrom;
    Button btnAddLocation, btnUploadImage, btnCancel, btnSave;
    Switch switch_event, switch_notification;

    public FragmentAddNewNotice() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.add_new_notice, container,
                false);
        context = getActivity();
        initilizeView(rootView);


        return rootView;
    }

    private void initilizeView(View mrootView) {
        editTextNoticeName = (EditText) mrootView.findViewById(R.id.editTextNoticeName);
        editTextDescription = (EditText) mrootView.findViewById(R.id.editTextDescription);
        btnUploadImage = (Button) mrootView.findViewById(R.id.btnUploadImage);
        btnCancel = (Button) mrootView.findViewById(R.id.btnCancel);
        btnSave = (Button) mrootView.findViewById(R.id.btnSave);
        btnAddLocation = (Button) mrootView.findViewById(R.id.btnAddLocation);
        textDateTo = (TextView) mrootView.findViewById(R.id.textDateTo);
        textDateFrom = (TextView) mrootView.findViewById(R.id.textDateFrom);
        textTimeTo = (TextView) mrootView.findViewById(R.id.textTimeTo);
        textTimeFrom = (TextView) mrootView.findViewById(R.id.textTimeFrom);
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        try {
            dialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
        }
        return dialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        try {
            // safety check
            if (getDialog() == null) {
                return;
            }

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;

            getDialog().getWindow().setLayout((int) (width * .98f), ViewGroup.LayoutParams.WRAP_CONTENT);


        } catch (Exception e) {
        }
    }


}
