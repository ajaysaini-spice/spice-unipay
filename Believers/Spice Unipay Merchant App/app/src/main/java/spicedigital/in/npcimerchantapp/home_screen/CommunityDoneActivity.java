package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.Fragments.FragmentAddMembers;
import spicedigital.in.npcimerchantapp.R;

public class CommunityDoneActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAddMember, btnHomePage;
    private TextView txtViewDynamicCommunityVpa, txtViewDynamicCommunityName;

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_community_done);

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        btnAddMember = (Button) findViewById(R.id.btnAddMembers);
        btnAddMember.setOnClickListener(this);

        btnHomePage = (Button) findViewById(R.id.btnHomePage);
        btnHomePage.setOnClickListener(this);

        txtViewDynamicCommunityVpa = (TextView) findViewById(R.id.txtViewDynamicCommunityVpa);
        txtViewDynamicCommunityName = (TextView) findViewById(R.id.txtViewDynamicCommunityName);


        txtViewDynamicCommunityName.setText(pref.getString("communityName", ""));
        txtViewDynamicCommunityVpa.setText(pref.getString("communityVPA", ""));

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnAddMembers) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            // Create and show the dialog.
            DialogFragment newFragment = new FragmentAddMembers();
            newFragment.show(ft, "dialog");

        } else if (view.getId() == R.id.btnHomePage) {
            startActivity(new Intent(CommunityDoneActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();

        }
    }

}
