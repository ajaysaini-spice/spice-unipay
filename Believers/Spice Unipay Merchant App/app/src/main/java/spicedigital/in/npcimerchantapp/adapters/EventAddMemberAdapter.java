package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.Fragments.EventAddMembers;
import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.Contact;

/**
 * Created by CH-E01006 on 3/19/2016.
 */

/**
 * Created by Temp-01421 on 19-03-2016.
 */
public class EventAddMemberAdapter extends RecyclerView.Adapter<EventAddMemberAdapter.ViewHolder> {

    private Context mContext;
    ArrayList<Contact> contactList;

    //    int checkCount;
    ArrayList<String> checkedPosList;

    public EventAddMemberAdapter(Context mContext, ArrayList<Contact> contactList) {
        super();

        this.mContext = mContext;
        this.contactList = contactList;

//        checkCount = 0;
        checkedPosList = new ArrayList<>();

        for(int i=0; i<contactList.size(); i++)
        {
            if(contactList.get(i).isChecked())
            {
                checkedPosList.add(""+i);
            }
        }

        Message msg = EventAddMembers.checkBoxHandlers.obtainMessage();
        msg.what = 1000;
        msg.obj = checkedPosList;
        EventAddMembers.checkBoxHandlers.sendMessage(msg);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.add_members_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Contact contact = contactList.get(position);
        holder.txtViewName.setText(contact.getName());
        holder.txtViewNumbers.setText(contact.getNumber());
        holder.selectContactCheckBox.setChecked(contact.isChecked());



        try {
            if(contact.getImageURI() == null)
            {
                holder.imageViewProfile.setImageResource(R.drawable.default_profile_img);
            }
            else
            {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), Uri.parse(contact.getImageURI()));
                holder.imageViewProfile.setImageBitmap(bitmap);
            }

        } catch (Exception e) {}

        holder.selectContactCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;
                contactList.get(position).setIsChecked(checkBox.isChecked());

                if(checkBox.isChecked())
                {
//                    checkCount++;
                    checkedPosList.add(""+position);
                }
                else
                {
//                    checkCount--;
                    checkedPosList.remove(""+position);
                }


                Message msg = EventAddMembers.checkBoxHandlers.obtainMessage();
                msg.what = 1000;
                msg.obj = checkedPosList;
                EventAddMembers.checkBoxHandlers.sendMessage(msg);

            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtViewName, txtViewNumbers;
        private ImageView imageViewProfile;
        private CheckBox selectContactCheckBox;

        public ViewHolder(View itemView) {
            super(itemView);
            txtViewName = (TextView) itemView.findViewById(R.id.txtViewName);
            imageViewProfile = (ImageView) itemView.findViewById(R.id.imageViewProfile);
            txtViewNumbers = (TextView) itemView.findViewById(R.id.txtViewNumbers);
            selectContactCheckBox = (CheckBox) itemView.findViewById(R.id.selectContactCheckBox);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }

}
