package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class CollectionEventDetail extends AppCompatActivity {


    Toolbar toolbar;
    View view;
    TextView titleText;

    TextView eventName, amount, eventCategory, eventId, typeTxt, fromDate, description, totalMember, perMemberAmount, totalAmount, unpaidAmount;

    Button viewMember, resendRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collection_event_detail);

        initialiseToolbar();
        initialiseviews();

    }

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {

        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("EVENT DETAIL");

        eventName = (TextView)findViewById(R.id.event_name);
        amount = (TextView)findViewById(R.id.amount);
        eventCategory = (TextView)findViewById(R.id.event_category);
        eventId = (TextView)findViewById(R.id.event_id);
        typeTxt = (TextView)findViewById(R.id.type);
        fromDate = (TextView)findViewById(R.id.from_date);
        description = (TextView)findViewById(R.id.description);
        totalMember = (TextView)findViewById(R.id.total_members);
        perMemberAmount = (TextView)findViewById(R.id.per_member_amount);
        totalAmount = (TextView)findViewById(R.id.total_amount);
        unpaidAmount = (TextView)findViewById(R.id.unpaid_amount);

        viewMember = (Button) findViewById(R.id.view_member);
        resendRequest = (Button) findViewById(R.id.resend_request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
