package spicedigital.in.npcimerchantapp.home_screen;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.NoticeBoardListAdapter;
import spicedigital.in.npcimerchantapp.support_classes.NoticeBoardBean;

/**
 * Created by ch-e00812 on 3/18/2016.
 */
public class NoticeBoardScreen extends AppCompatActivity {


    ListView mListView;
    Toolbar toolbar;
    View view;
    TextView titleText;
    ArrayList<NoticeBoardBean> mDataList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notice_board_screen);


        setData();

        initialiseToolbar();
        initialiseviews();



    }


    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {

        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("NOTICE BOARD");
        mListView = (ListView) findViewById(R.id.listView1);

        mListView.setAdapter(new NoticeBoardListAdapter(this, mDataList));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void setData(){

        mDataList = new ArrayList<>();

        NoticeBoardBean bean = new NoticeBoardBean();
        bean.eventName = "Happy Holi";
        bean.dateTime = "24 Mar at 6:00PM";
        bean.description = "Happy Holi to All Members !!!";
        bean.location = "";
        bean.address1 = "";
        bean.address2 = "";
        bean.address3 = "";
        bean.postedOn = "Posted on 20 Mar";
        mDataList.add(bean);

        bean = new NoticeBoardBean();
        bean.eventName = "Lets Meet at Party Hall";
        bean.dateTime = "22 Mar at 6:00PM";
        bean.description = "Lets meet at the our hall for the meeting about the holi festival. Lets meet and discuss the budget for the celebration.";
        bean.location = "";
        bean.address1 = "Party Hall,";
        bean.address2 = "#42, Fine House Appartments,";
        bean.address3 = "New Delhi";
        bean.postedOn = "Posted on 19 Mar";
        mDataList.add(bean);

        bean = new NoticeBoardBean();
        bean.eventName = "Genset Replacement";
        bean.dateTime = "3 Mar at 6:00PM";
        bean.description = "We need to buy a new genset. As our old genset is not working. New Genset costs is Rs 30,000.";
        bean.location = "";
        bean.address1 = "";
        bean.address2 = "";
        bean.address3 = "";
        bean.postedOn = "Posted on 1 Mar";
        mDataList.add(bean);

        bean = new NoticeBoardBean();
        bean.eventName = "Quaterly Membership Fee";
        bean.dateTime = "28 Feb at 6:00PM";
        bean.description = "Lets meet at the our hall for the meeting about membership fee. Lets meet and discuss.";
        bean.location = "";
        bean.address1 = "Party Hall,";
        bean.address2 = "#42, Fine House Appartments,";
        bean.address3 = "New Delhi";
        bean.postedOn = "Posted on 26 Feb";
        mDataList.add(bean);


        bean = new NoticeBoardBean();
        bean.eventName = "New Year Party";
        bean.dateTime = "1 Jan at 6:00PM";
        bean.description = "Lets Rock and Roll at the new year party.";
        bean.location = "";
        bean.address1 = "Party Hall,";
        bean.address2 = "#42, Fine House Appartments,";
        bean.address3 = "New Delhi";
        bean.postedOn = "Posted on 27 Dec";
        mDataList.add(bean);


    }


}
