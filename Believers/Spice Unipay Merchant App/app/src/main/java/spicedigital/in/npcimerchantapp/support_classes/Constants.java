package spicedigital.in.npcimerchantapp.support_classes;

/**
 * Created by CH-E01006 on 3/19/2016.
 */
public class Constants {

    public final static String isShowOTP = "false";
    public final static String isShowCreateGroup = "false";
    public final static String mobileNumber = "mobileNumber";
    public final static String isMainActivityOpened = "isMainActivityShown";

}
