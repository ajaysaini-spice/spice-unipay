package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

public class JoinCommunityActivity extends AppCompatActivity implements View.OnClickListener {

    private Button cancelCommunityButton, btnJoinCommunity;
    private TextView txtViewDynamicCommunityName, txtViewCommunityType, textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_layout);
        try{
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinCommunityActivity.this);
        cancelCommunityButton = (Button) findViewById(R.id.cancelCommunityButton);
        cancelCommunityButton.setOnClickListener(this);

        btnJoinCommunity = (Button) findViewById(R.id.btnJoinCommunity);
        btnJoinCommunity = (Button) findViewById(R.id.btnJoinCommunity);

        txtViewDynamicCommunityName = (TextView) findViewById(R.id.txtViewDynamicCommunityName);
        txtViewCommunityType = (TextView) findViewById(R.id.txtViewCommunityType);
        textViewDescription = (TextView) findViewById(R.id.textViewDescription);

        String Name = sharedPreferences.getString("communityType", "");
            String type = sharedPreferences.getString("communityName", "");

            if(!Name.equalsIgnoreCase("")) {
                txtViewDynamicCommunityName.setText(Name);
            }

            if(!type.equalsIgnoreCase("")) {
                txtViewCommunityType.setText(type);
            }

        btnJoinCommunity.setOnClickListener(this);
        cancelCommunityButton.setOnClickListener(this);
    }catch(Exception e){
        e.printStackTrace();
    }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cancelCommunityButton) {
            finish();

        } else if (view.getId() == R.id.btnJoinCommunity) {



            startActivity(new Intent(JoinCommunityActivity.this, JoinGroupDetailActivity.class));
        }
    }
}
