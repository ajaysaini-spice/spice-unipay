package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.MemberBean;

/**
 * Created by CH-E01062 on 19-03-2016.
 */
public class MemberListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<MemberBean> mList;

    public MemberListAdapter(Context context, ArrayList<MemberBean> list) {

        mList = list;
        mContext = context;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.member_list_item, null);
            holder = new ViewHolder();

            holder.status = (TextView) convertView.findViewById(R.id.status);

            holder.personImage = (ImageView) convertView.findViewById(R.id.personImage);

            holder.phoneNum = (TextView) convertView.findViewById(R.id.phone_num);
            holder.personName = (TextView) convertView.findViewById(R.id.person_name);

            holder.statusBackground = (FrameLayout) convertView.findViewById(R.id.status_background);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.personName.setText(mList.get(position).personName);
        holder.phoneNum.setText(mList.get(position).mobileNumber);


        if (mList.get(position).amountStatus.equalsIgnoreCase("Paid")) {
            holder.status.setText("Paid");
            holder.personImage.setImageResource(R.drawable.image);
            holder.statusBackground.setBackgroundColor(mContext.getResources().getColor(R.color.paymentApproveColor));
        } else {
            holder.personImage.setImageResource(R.drawable.image1);

            holder.status.setText("Not Paid");
            holder.statusBackground.setBackgroundColor(mContext.getResources().getColor(R.color.paymentRejectColor));

        }


        return convertView;
    }

    static class ViewHolder {
        private TextView status, phoneNum, personName;
        private FrameLayout statusBackground;
        private ImageView personImage;

    }
}
