package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.database.MyDataBase;

/**
 * Created by CH-E01062 on 18-03-2016.
 */
public class CollectionRequestAdapter extends BaseAdapter {


    Context mContext;
    ArrayList<HashMap<String, String>> mList;

    public CollectionRequestAdapter(Context context,ArrayList<HashMap<String, String>> list) {

        mList = list;
        mContext = context;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.collection_reuests_list_item, null);
            holder = new ViewHolder();
            holder.fromDate = (TextView) convertView.findViewById(R.id.from_date);
            holder.toDate = (TextView) convertView.findViewById(R.id.to_date);
            holder.numMembers = (TextView) convertView.findViewById(R.id.num_members);

            holder.nonPayee = (TextView) convertView.findViewById(R.id.non_payee);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);
            holder.eventNmae = (TextView) convertView.findViewById(R.id.event_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> recordData = mList.get(position);

        holder.eventNmae.setText(recordData.get(MyDataBase.KEY_EVENT_NAME));

        holder.fromDate.setText(recordData.get(MyDataBase.KEY_EVENT_TIMESTAMP));
        holder.amount.setText(mContext.getResources().getString(R.string.rupees)+" "+recordData.get(MyDataBase.KEY_AMOUNT));

        return convertView;
    }

    static class ViewHolder {
        private TextView fromDate, toDate, numMembers, nonPayee, amount, eventNmae;

    }
}
