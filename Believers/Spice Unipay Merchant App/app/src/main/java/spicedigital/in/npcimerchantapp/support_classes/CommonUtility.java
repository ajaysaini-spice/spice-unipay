package spicedigital.in.npcimerchantapp.support_classes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by CH-E01006 on 3/19/2016.
 */
public class CommonUtility {


    public static void showSimpleSnackBar(Context context, String message, CoordinatorLayout coordinatorLayout) {
        try {
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, message, Snackbar.LENGTH_LONG).setDuration(Snackbar.LENGTH_LONG);
            try {
                TextView tv = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
                Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
                tv.setTypeface(font);
            } catch (Exception e) {

            }
            snackbar.show();
        } catch (Exception e) {
            displayToast(context, message, 1);

        }
    }


    /**
     * Display toast message
     *
     * @param mContext
     * @param message
     */
    public static void displayToast(Context mContext, String message,
                                    int numberOfTimes) {
        int count = 0;
        while (count < numberOfTimes) {
            ++count;
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("NewApi")
    public static void setStatusBarColor(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((Activity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(context.getResources().getColor(
                        R.color.transparentcolor));
            }
        } catch (Exception e) {
        }
    }

}
