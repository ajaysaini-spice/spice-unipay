package spicedigital.in.npcimerchantapp.home_screen;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import spicedigital.in.npcimerchantapp.R;

public class JoinGroupDetailActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnSubmit;


    private EditText edittextName, editTextGender, editTextVPA, editTextAccountNo, editTextMMID, editTextIFCCode, editTextAdharCodeNo;
    private SharedPreferences sharedPreferences;
    private Context context;
    private TextView editTextDOB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_details);
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        initializeViews();
        setOnClickListener();

    }

    private void setOnClickListener() {
        btnSubmit.setOnClickListener(this);
        editTextDOB.setOnClickListener(this);
    }

    public void initializeViews() {
        try {
            btnSubmit = (Button) findViewById(R.id.btnSubmit);
            edittextName = (EditText) findViewById(R.id.edittextName);
            editTextDOB = (TextView) findViewById(R.id.editTextDOB);
            editTextGender = (EditText) findViewById(R.id.editTextGender);
            editTextVPA = (EditText) findViewById(R.id.editTextVPA);
            editTextAccountNo = (EditText) findViewById(R.id.editTextAccountNo);
            editTextMMID = (EditText) findViewById(R.id.editTextMMID);
            editTextIFCCode = (EditText) findViewById(R.id.editTextIFCCode);
        } catch (Exception e) {

        }

    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {


            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(JoinGroupDetailActivity.this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("memberAcc", editTextAccountNo.getText().toString());
            editor.putString("membername", edittextName.getText().toString());
            editor.putString("memberVPA", editTextVPA.getText().toString());

            editor.putString("PersonType", "MEMBER");
            editor.commit();

            if (edittextName.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your First Name", Toast.LENGTH_SHORT).show();
            } /*else if (editTextDOB.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Date Of Birth", Toast.LENGTH_SHORT).show();
            } else if (editTextGender.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Gender", Toast.LENGTH_SHORT).show();
            } */else if (editTextVPA.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your VPA", Toast.LENGTH_SHORT).show();
            } else if (editTextAccountNo.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your Account Number", Toast.LENGTH_SHORT).show();
            } /*else if (editTextMMID.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your MMID", Toast.LENGTH_SHORT).show();
            } else if (editTextIFCCode.getText().toString().length() == 0) {
                Toast.makeText(this, "Enter Your IFC Code", Toast.LENGTH_SHORT).show();
            } */else {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("First name", edittextName.getText().toString());
                edit.putString("Date Of Birth", editTextDOB.getText().toString());
                edit.putString("Gender", editTextGender.getText().toString());
                edit.putString("VPA", editTextVPA.getText().toString());
                edit.putString("Account Number", editTextAccountNo.getText().toString());
                edit.putString("MMID", editTextMMID.getText().toString());
                edit.putString("IFC Code", editTextIFCCode.getText().toString());
                edit.commit();

                startActivity(new Intent(JoinGroupDetailActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));

            }
        }
        else if (v == editTextDOB){
            DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {

                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    myCalendar.set(Calendar.YEAR, year);

                    updatedate(year, monthOfYear, dayOfMonth);


                }
            };

            Calendar myCalendar = Calendar.getInstance();

            new DatePickerDialog(context, d,
                    myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    protected void updatedate(int yr, int mon, int day) {
        try {



            Date toDate, fromDate = null;


            String dtStart = yr + "/" + (mon + 1) + "/" + day;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            try {
                fromDate = format.parse(dtStart);

                dtStart = format.format(fromDate);

            } catch (Exception e) {
                // TODO Auto-generated catch block

            }




            editTextDOB.setText(dtStart);








        } catch (Exception e) {

        }
    }
}