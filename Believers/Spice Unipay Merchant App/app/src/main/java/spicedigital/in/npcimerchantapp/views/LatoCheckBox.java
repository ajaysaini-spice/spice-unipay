package spicedigital.in.npcimerchantapp.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by CH-E01073 on 19-03-2016.
 */

public class LatoCheckBox extends CheckBox {

    public LatoCheckBox(Context context) {
        super(context);
        style(context);
    }

    public LatoCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public LatoCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        try {
            Typeface tf = Typeface.createFromAsset(context.getAssets(),
                    "fonts/Lato-Regular.ttf");
            setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}