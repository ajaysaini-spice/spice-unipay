package spicedigital.in.npcimerchantapp.home_screen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.support_classes.Constants;
import spicedigital.in.npcimerchantapp.views.CollectionActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    LinearLayout layoutName, bannerView, paymentRequestView, collectionRequestView;
    FrameLayout walletView, memberView;
    LinearLayout offerView, expenses_graph;
    ImageView calenderView, categoryImage;
    Toolbar toolbar;
    View view;
    TextView titleText, walletBalance, personName;
    ImageView menuIconToolbar;

   String PersonType="";

    ImageView noticeBoard;
    SharedPreferences pref;

    TextView textViewPayment,textViewPayment2;
    TextView txtViewmoney,txtCompleted;
    TextView txtViewmoney2,txtCompleted2;


    Integer[] calendarArr = new Integer[]{
            R.drawable.calender,
            R.drawable.calender_21,
            R.drawable.calender_22,
            R.drawable.calender_23,
            R.drawable.calender_24,
            R.drawable.calender_25,
            R.drawable.calender_26,
            R.drawable.calender_27
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        PersonType = sharedPreferences.getString("PersonType","");


        initializeViews();
        setOnClickListener();

        if(!pref.getBoolean(Constants.isMainActivityOpened, false))
        {
            insertData();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean(Constants.isMainActivityOpened, true);
            edit.commit();
        }

    }

    public void insertData(){
        try{
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

         /*   AppControler.getDataBaseInstance().addNotification("12345","5457","ICCTW20","Sports","","member","member","society name","society","20000","",
                    "19 March 2016","Pending","","","","","");

            AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12345", "5457", "Fuel Charges", "Bills", "", "society", "society", "merchant name", "merchant", "80000", "",
                    "18 March 2016", "Pending", "", "", "", "", "");*/


            if(PersonType.equalsIgnoreCase("ADMIN")) {

            /*
                AppControler.getDataBaseInstance().addAdminCollectionRequest("12345", "5457", "Monthly Charges", "Fund", "", "member", "member", "society name", "society", "80000", "",
                        "23 March 2016", "Pending", "", "", "", "", "");*/

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12346", "5458", "Property Tax", "Tax", "20 March 2016", "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),  "30000", "",
                        "20 March 2016", "Pending", "500", "300", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12347", "5459", "Genset Replacement", "Maintenance", "22 March 2016",  "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),  "30000", "",
                        "22 March 2016", "Pending", "500", "200", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12348", "5460", "Membership Fees", "Funds", "23 March 2016",  "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),  "25000", "",
                        "23 March 2016", "Pending", "500", "100", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12349", "5461", "Water Bill", "Bills", "26 March 2016",  "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),  "15000", "",
                        "26 March 2016", "Pending", "500", "300", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016",  "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),  "40000", "",
                        "27 March 2016", "Pending", "500", "150", "", "", "");

                AppControler.getDataBaseInstance().addAdminCollectionRequest("12351", "5463", "New Year Party", "Party", "30 March 2016",  "", "",sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "30000", "",
                        "30 March 2016", "Pending", "500", "100", "", "", "");


                AppControler.getDataBaseInstance().addNotification("12346", "5458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Paid", "500", "300","Singla", "", "");

                AppControler.getDataBaseInstance().addNotification("12347", "5459", "Genset Replacement", "Maintenance", "22 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Rejected", "500", "200", "Kings", "", "");

                AppControler.getDataBaseInstance().addNotification("12348", "5460", "Membership Fees", "Funds", "23 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Paid", "500", "100", "Ankur", "", "");

                AppControler.getDataBaseInstance().addNotification("12349", "5461", "Water Bill", "Bills", "26 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),"Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", "Singh", "", "");

                AppControler.getDataBaseInstance().addNotification("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Rejected", "500", "150", "Nishu", "", "");

                AppControler.getDataBaseInstance().addNotification("12351", "5463", "New Year Party", "Party", "30 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),"Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Paid", "500", "100", "R K Verma", "", "");
            }else{

                AppControler.getDataBaseInstance().addNotification("23346", "15458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Pending", "500", "300", "Sachin", "", "");

                AppControler.getDataBaseInstance().addNotification("23347", "15459", "Genset Replacement", "Maintenance", "21 March 2016",  sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Pending", "500", "200", "Mark", "", "");

                AppControler.getDataBaseInstance().addNotification("23348", "15460", "Membership Fees", "Funds", "23 March 2016",  sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Pending", "500", "100", "J K", "", "");

                AppControler.getDataBaseInstance().addNotification("22349", "15461", "Water Bill", "Bills", "26 March 2016",  sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", "V Sharama", "", "");

                AppControler.getDataBaseInstance().addNotification("22350", "15462", "Severage Charges", "Maintenance", "27 March 2016",  sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""),"Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Pending", "500", "150", "Singla", "", "");

                AppControler.getDataBaseInstance().addNotification("22351", "5463", "New Year Party", "Party", "30",  sharedPreferences.getString("membername", ""), sharedPreferences.getString("memberVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Pending", "500", "100", "Ankur", "", "");



                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12346", "5458", "Property Tax", "Tax", "20 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Wave Town", "WaveTown", "30000", "",
                        "20 March 2016", "Paid", "500", "300",sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12347", "5459", "Genset Replacement", "Maintenance", "21 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "30000", "",
                        "22 March 2016", "Rejected", "500", "200", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12348", "5460", "Membership Fees", "Funds", "23 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Sunny Enclave", "SunEnclave", "25000", "",
                        "23 March 2016", "Paid", "500", "100", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12349", "5461", "Water Bill", "Bills", "26 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),"Sun City", "SunCity", "15000", "",
                        "26 March 2016", "Pending", "500", "300", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12350", "5462", "Severage Charges", "Maintenance", "27 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""), "Modern Colony", "ModColony", "40000", "",
                        "27 March 2016", "Rejected", "500", "150", sharedPreferences.getString("First name", ""), "", "");

                AppControler.getDataBaseInstance().addMemberApprovalPayRequest("12351", "5463", "New Year Party", "Party", "30 March 2016", sharedPreferences.getString("name", ""),sharedPreferences.getString("communityVPA", ""),"Wave Town", "WaveTown", "30000", "",
                        "30 March 2016", "Paid", "500", "100", sharedPreferences.getString("First name", ""), "", "");

            }

        }catch(Exception e){

        }
    }

    public void setOnClickListener() {
        layoutName.setOnClickListener(this);
        walletView.setOnClickListener(this);
        memberView.setOnClickListener(this);
        calenderView.setOnClickListener(this);
        paymentRequestView.setOnClickListener(this);
        collectionRequestView.setOnClickListener(this);
        offerView.setOnClickListener(this);
        bannerView.setOnClickListener(this);


    }

    public void initializeViews() {


        categoryImage = (ImageView) findViewById(R.id.category_image);


        pref = PreferenceManager.getDefaultSharedPreferences(this);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        view = toolbar.getRootView();
        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("GREEN HOUSE SOCIETY");

        personName = (TextView) findViewById(R.id.person_name);


        textViewPayment = (TextView) findViewById(R.id.textViewPayment);
        textViewPayment2 = (TextView) findViewById(R.id.textViewPayment2);

        txtViewmoney = (TextView) findViewById(R.id.txtViewmoney);
        txtCompleted = (TextView) findViewById(R.id.txtCompleted);

        txtViewmoney2 = (TextView) findViewById(R.id.txtViewmoney2);
        txtCompleted2 = (TextView) findViewById(R.id.txtCompleted2);
        if(PersonType.equalsIgnoreCase("ADMIN")){
            textViewPayment.setText("Payment Requests");
                    textViewPayment2.setText("Collection Requests");
            txtViewmoney.setText("20");
            txtCompleted.setText("Completed");
            txtViewmoney2.setText("47");
            txtCompleted2.setText("Pending");
            personName.setText("Mr. "+pref.getString("name", ""));
        }else{
            textViewPayment.setText("Initiate Payments");
            textViewPayment2.setText("Payment Requests");
            txtViewmoney2.setText("20");
            txtCompleted2.setText("Completed");
            txtViewmoney.setText("47");
            txtCompleted.setText("Pending");
            personName.setText("Mr. "+pref.getString("membername", ""));
        }

        noticeBoard = (ImageView) view.findViewById(R.id.notice_board);
        noticeBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NoticeBoardScreen.class));
            }
        });
        titleText.setText(pref.getString("communityType", ""));

        walletBalance = (TextView) findViewById(R.id.wallet_balance);
        walletBalance.setText(getResources().getString(R.string.rupees) + "12,040");

        bannerView = (LinearLayout) findViewById(R.id.banner);
        menuIconToolbar = (ImageView) view.findViewById(R.id.menu_icon_toolbar);
        menuIconToolbar.setVisibility(View.VISIBLE);
        layoutName = (LinearLayout) findViewById(R.id.layout_name);
        walletView = (FrameLayout) findViewById(R.id.wallet_view);
        memberView = (FrameLayout) findViewById(R.id.member_view);
        calenderView = (ImageView) findViewById(R.id.calendar1);
        paymentRequestView = (LinearLayout) findViewById(R.id.payment_request_view);
        collectionRequestView = (LinearLayout) findViewById(R.id.collection_request_view);
        offerView = (LinearLayout) findViewById(R.id.offer_view);
        expenses_graph = (LinearLayout) findViewById(R.id.expenses_graph);
        expenses_graph.setOnClickListener(this);
        setOnClickListener();
        setCategoryImage();
        setCalendarImage();
    }

    private void setCategoryImage() {

        if (pref.getString("communityType", "").equalsIgnoreCase("Housing Society")) {
            categoryImage.setImageResource(R.drawable.housing);
        } else if (pref.getString("communityType", "").equalsIgnoreCase("NGO")) {
            categoryImage.setImageResource(R.drawable.ngo);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("School")) {
            categoryImage.setImageResource(R.drawable.school);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Self help group")) {
            categoryImage.setImageResource(R.drawable.self);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Microsofinance industry")) {
            categoryImage.setImageResource(R.drawable.finance);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("Micro insurance provider")) {
            categoryImage.setImageResource(R.drawable.insurance);

        } else if (pref.getString("communityType", "").equalsIgnoreCase("NBFC")) {
            categoryImage.setImageResource(R.drawable.nbfc);

        }

    }


    private void setCalendarImage(){

        DateFormat dateFormat = new SimpleDateFormat("dd");
        Date date = new Date();
        String todayDay = dateFormat.format(date);

        if (todayDay.equalsIgnoreCase("19")){
            calenderView.setImageResource(calendarArr[0]);
        }
        else if (todayDay.equalsIgnoreCase("20")){
            calenderView.setImageResource(calendarArr[1]);
        }
        else if (todayDay.equalsIgnoreCase("21")){
            calenderView.setImageResource(calendarArr[1]);
        }
        else if (todayDay.equalsIgnoreCase("22")){
            calenderView.setImageResource(calendarArr[2]);
        }
        else if (todayDay.equalsIgnoreCase("23")){
            calenderView.setImageResource(calendarArr[3]);
        }
        else if (todayDay.equalsIgnoreCase("24")){
            calenderView.setImageResource(calendarArr[4]);
        }
        else if (todayDay.equalsIgnoreCase("25")){
            calenderView.setImageResource(calendarArr[5]);
        }
        else if (todayDay.equalsIgnoreCase("26")){
            calenderView.setImageResource(calendarArr[6]);
        }
        else if (todayDay.equalsIgnoreCase("27")){
            calenderView.setImageResource(calendarArr[7]);
        }


    }


    @Override
    public void onClick(View v) {

        if (v == collectionRequestView) {


//            new FragmentCreateCommunity().show(getSupportFragmentManager(), "TAG");

              if(PersonType.equalsIgnoreCase("ADMIN")){
              startActivity(new Intent(this, CollectionActivity.class));
             }else{
              startActivity(new Intent(this, PaymentRequestforMembers.class));
              }
            //sendCollectRequest();

        } else if (v == bannerView) {
            startActivity(new Intent(this, NoticeBoardScreen.class));
        } else if (v == offerView) {

        } else if (v == layoutName) {
            startActivity(new Intent(this, MyProfileScreen.class));
        } else if (v == paymentRequestView) {

                startActivity(new Intent(this, PaymentRequestiesActivity.class));


            // sendRequesToPSPApp();


        } else if (v == memberView) {

            startActivity(new Intent(this, MemberViewActivity.class));


        } else if (v == calenderView) {
            startActivity(new Intent(this, CalendarScreen.class));
        } else if (v == expenses_graph) {
            startActivity(new Intent(this, ActivityDashboard.class));


        }


    }

    public void sendCollectRequest() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        HashMap parameters = new HashMap();
        //  parameters.put("mobilenumber", "7529841084,9888762945");

        parameters.put("aryOfNumbers", "9888762945,7529841084");
        parameters.put("payeeName", sharedPreferences.getString("name", ""));
        parameters.put("payeeAddress", sharedPreferences.getString("communityVPA", ""));
        parameters.put("eventType", "hello gghh");
        parameters.put("eventName", "hello gghh");
        parameters.put("amount", "1000");
        parameters.put("startDate", "12-02-899");
        parameters.put("endDate", "13-89-77");
        parameters.put("timeStamp", "45-88-44");
        parameters.put("eventDesc", "hjfjwj fjkkf");
        parameters.put("mobileNumber", "8043240832");
        parameters.put("walletId", System.currentTimeMillis());

        ParseCloud.callFunctionInBackground("push_notification_collect", parameters, new FunctionCallback<Object>() {
            public void done(Object response, ParseException e) {
                if (e == null) {
                    Toast.makeText(MainActivity.this, "Notification has been sent successfully", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Failed, Please try again later", Toast.LENGTH_LONG).show();

                }
            }
        });

    }


    public void sendRequesToPSPApp() {
        String DEEPLINKING_URL_BASE = "upi://pay";
        StringBuilder urlBuilder = new StringBuilder();
        int orderId = 100001 % new Random().nextInt();


        JSONObject payload2 = new JSONObject();
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

            payload2.put("amount", "100 ");
            payload2.put("payeeName", "");
            payload2.put("payeeAddress", "");
            payload2.put("payerAddress", "");
            payload2.put("payerName", "");
            payload2.put("mobileNumber", sharedPreferences.getString(Constants.mobileNumber, ""));
        } catch (Exception e) {

        }

        //   urlBuilder.append("upi://pay").append("?").append("amount").append("=").append("100").append("&").append("ORDERID").append("=").append(Integer.parseInt("100")).append("&").append("credit_flag").append("=").append("1").append("&").append("appname").append("=").append("MGSAPP").append("&").append("am").append("=").append("100").append("&").append("tn").append("=").append(Integer.parseInt("100")).append("&").append("tr").append("=").append("Test for UPI").append("&").append("ti").append("=").append(Integer.parseInt("1000")).append("&").append("appid").append("=").append("1116").append("&").append("cu").append("=").append("INR").append("&").append("gcode").append("=").append("123466454").append("&").append("location").append("=").append("Mumbai,Maharashtra").append("&").append("ip").append("=").append("154fer53urn").append("&").append("os").append("=").append("android").append("&").append("payerAcAddressType").append("=").append("100").append("&").append("capability").append("=").append("453453d4f5343434df354").append("&").append("payeeName").append("=").append("890").append("&").append("payeeType").append("=").append("PERSON").append("&").append("PVA").append("=").append("100" + "@mapp").append("&").append("PayerVA").append("=").append("100");
        urlBuilder.append("upi://pay").append("?").append("request").append("=").append(payload2.toString()).append("&").append("TYPE").append("=").append("collect");

        String deepLinkUrl = urlBuilder.toString();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(deepLinkUrl));
        //  intent.putExtra("data",deepLinkUrl);
        Intent chooser = Intent.createChooser(intent, "Pay With");
        if (intent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
            MainActivity.this.startActivityForResult(chooser, 1);
        }


    }

    private class LoadingTask1 extends AsyncTask<Void, Void, Void> {
        ProgressDialog pDialog;

        private LoadingTask1() {
        }

        protected void onPreExecute() {
            this.pDialog = new ProgressDialog(MainActivity.this);
            this.pDialog.setMessage("Loading...");
            this.pDialog.show();
        }

        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(5000);
            } catch (Exception e) {
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            String DEEPLINKING_URL_BASE = "upi://pay";
            StringBuilder urlBuilder = new StringBuilder();
            int orderId = 100001 % new Random().nextInt();
            urlBuilder.append("upi://pay").append("?").append("amount").append("=").append("100").append("&").append("ORDERID").append("=").append(Integer.parseInt("100")).append("&").append("credit_flag").append("=").append("1").append("&").append("appname").append("=").append("MGSAPP").append("&").append("am").append("=").append("100").append("&").append("tn").append("=").append(Integer.parseInt("100")).append("&").append("tr").append("=").append("Test for UPI").append("&").append("ti").append("=").append(Integer.parseInt("1000")).append("&").append("appid").append("=").append("1116").append("&").append("cu").append("=").append("INR").append("&").append("gcode").append("=").append("123466454").append("&").append("location").append("=").append("Mumbai,Maharashtra").append("&").append("ip").append("=").append("154fer53urn").append("&").append("os").append("=").append("android").append("&").append("payerAcAddressType").append("=").append("100").append("&").append("capability").append("=").append("453453d4f5343434df354").append("&").append("payeeName").append("=").append("890").append("&").append("payeeType").append("=").append("PERSON").append("&").append("PVA").append("=").append("100" + "@mapp").append("&").append("PayerVA").append("=").append("100");
            String deepLinkUrl = urlBuilder.toString();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(deepLinkUrl));
            Intent chooser = Intent.createChooser(intent, "Pay With");
            if (intent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
                MainActivity.this.startActivityForResult(chooser, 1);
            }
            this.pDialog.dismiss();
        }
    }
}
