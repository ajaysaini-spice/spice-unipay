package spicedigital.in.npcimerchantapp.home_screen;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseInstallation;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.CommonUtility;
import spicedigital.in.npcimerchantapp.support_classes.Constants;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextMobileNumber;
    private Button buttonSubmit;
    private Context context;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secure_otp_layout);
        CommonUtility.setStatusBarColor(OtpActivity.this);
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        /*if (sharedPreferences.getBoolean(Constants.isShowOTP, false)) {
            startActivity(new Intent(context, CreateJoinActivity.class));
            finish();
        }else  if(sharedPreferences.getBoolean(Constants.isShowCreateGroup, false)){
            startActivity(new Intent(context, CommunityDoneActivity.class));
            finish();
        }*/

        editTextMobileNumber = (EditText) findViewById(R.id.editTextMobileNo);
        buttonSubmit = (Button) findViewById(R.id.btnSubmit);
        buttonSubmit.setOnClickListener(this);
    }

    public void addFieldsInInstallation() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("mobileNumber", editTextMobileNumber.getText().toString());
        installation.saveInBackground();
    }

    @Override
    public void onClick(View v) {
        if (v == buttonSubmit) {

            if(validation()) {
                addFieldsInInstallation();

                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putBoolean(Constants.isShowOTP, true);
                edit.putString(Constants.mobileNumber, editTextMobileNumber.getText().toString());
                edit.commit();
                startActivity(new Intent(context, CreateJoinActivity.class));
            }
        }
    }

    public Boolean validation() {
        try {
            String mobileNo = editTextMobileNumber.getText().toString().trim();
            if (mobileNo.equalsIgnoreCase("")) {
                Toast.makeText(OtpActivity.this,
                        "Please enter mobile number.",
                        Toast.LENGTH_LONG).show();
                editTextMobileNumber.requestFocus();
                return false;
            } else if (mobileNo.startsWith("0")) {
                Toast.makeText(OtpActivity.this,
                        "Mobile number cannot start with 0.",
                        Toast.LENGTH_LONG).show();
                editTextMobileNumber.requestFocus();
                return false;
            } else if (mobileNo.length() > 10 || mobileNo.length() < 10) {
                Toast.makeText(OtpActivity.this,
                        "You have entered incorrect mobile number.",
                        Toast.LENGTH_LONG).show();
                editTextMobileNumber.requestFocus();
                return false;
            }



    }catch(Exception e){
        e.printStackTrace();
    }
        return true;
    }
}
