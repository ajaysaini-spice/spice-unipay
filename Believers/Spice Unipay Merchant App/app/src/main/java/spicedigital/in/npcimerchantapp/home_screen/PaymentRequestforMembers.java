package spicedigital.in.npcimerchantapp.home_screen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.Fragments.FragmentAddNewPayment;
import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.adapters.CollectionRequestScreenAdapter;
import spicedigital.in.npcimerchantapp.application.AppControler;

/**
 * Created by CH-E01062 on 20-03-2016.
 */
public class PaymentRequestforMembers extends AppCompatActivity implements View.OnClickListener {

    ListView mListView;
    CollectionRequestScreenAdapter adapter;

    ArrayList mDataList;
    Toolbar toolbar;
    View view;
    TextView titleText;
    ImageView fabBtn;
    String PersonType="";
    ArrayList<HashMap<String, String>> getpaymentrequest;

    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collection_requests);
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());

        PersonType = sharedPreferences.getString("PersonType","");
        initialiseToolbar();
        initialiseviews();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void initialiseviews() {


        fabBtn = (ImageView) findViewById(R.id.fab_btn);
        fabBtn.setOnClickListener(this);

        titleText = (TextView) view.findViewById(R.id.title_text);

        titleText.setText("PAYMENT REQUESTS");
        fabBtn.setVisibility(View.GONE);


        mDataList = new ArrayList();
        mDataList.add("1");
        mDataList.add("1");
        mDataList.add("1");
        mDataList.add("1");
        mListView = (ListView) findViewById(R.id.collection_request_list);

       // adapter = new CollectionRequestScreenAdapter(this, mDataList);
       // mListView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        if (v == fabBtn) {


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{

            getpaymentrequest = new ArrayList<HashMap<String, String>>();

            getpaymentrequest = AppControler.getDataBaseInstance().getNotificationList();
            if(getpaymentrequest.size() > 0) {
                adapter = new CollectionRequestScreenAdapter(this, getpaymentrequest);
                mListView.setAdapter(adapter);
            }



        }catch(Exception e){

        }
    }

    public void callPayRequest(HashMap<String, String> dataSelected){
        try {
            FragmentAddNewPayment fragmentAddNewPayment = new FragmentAddNewPayment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("recordData", dataSelected);
            fragmentAddNewPayment.setArguments(bundle);
            fragmentAddNewPayment.show(getSupportFragmentManager(), "");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
