package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.NoticeBoardBean;

/**
 * Created by CH-E01062 on 19-03-2016.
 */
public class NoticeBoardListAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<NoticeBoardBean> mList;

    public  NoticeBoardListAdapter(Context context, ArrayList<NoticeBoardBean> list) {

        mContext = context;
        mList = list;

    }



    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        ViewHolder holder;

        if (v == null) {

            LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inf.inflate(R.layout.custom_notice_board, null);
            holder = new ViewHolder();

            holder.eventName = (TextView) v.findViewById(R.id.event_name);
            holder.dateTimeTxt = (TextView) v.findViewById(R.id.date_time_txt);
            holder.description = (TextView) v.findViewById(R.id.description);
            holder.address1 = (TextView) v.findViewById(R.id.address1);
            holder.address2 = (TextView) v.findViewById(R.id.address2);
            holder.address3 = (TextView) v.findViewById(R.id.address3);
            holder.postedOn = (TextView) v.findViewById(R.id.posted_on);
            holder.addressLay = (LinearLayout) v.findViewById(R.id.address_lay);

            v.setTag(holder);

        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.eventName.setText(mList.get(position).eventName);
        holder.dateTimeTxt.setText(mList.get(position).dateTime);
        holder.description.setText(mList.get(position).description);
        holder.postedOn.setText(mList.get(position).postedOn);
        if(mList.get(position).address1.trim().equalsIgnoreCase(""))
        {
            holder.addressLay.setVisibility(View.GONE);
        }
        else
        {
            holder.addressLay.setVisibility(View.VISIBLE);
            holder.address1.setText(mList.get(position).address1);
            holder.address2.setText(mList.get(position).address2);
            holder.address3.setText(mList.get(position).address3);
        }



        return v;
    }

    class ViewHolder {
        TextView eventName;
        TextView dateTimeTxt;
        TextView description;
        TextView address1;
        TextView address2;
        TextView address3;
        TextView postedOn;
        LinearLayout addressLay;
    }

}