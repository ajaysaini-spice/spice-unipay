package spicedigital.in.npcimerchantapp.home_screen;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.support_classes.Constants;

/**
 * Created by Temp-01421 on 18-03-2016.
 */
public class CreateJoinActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnJoinCommunity, btnCreateCommunity;
    private TextView textViewTitle;
//    private ProgressBar progressBar;
    private LinearLayout LinearLayoutTop;
    private ImageView imageViewIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_join_layout);



        btnJoinCommunity = (Button) findViewById(R.id.btnJoinCommunity);
        btnCreateCommunity = (Button) findViewById(R.id.btnCreateCommunity);
        LinearLayoutTop = (LinearLayout) findViewById(R.id.LinearLayoutTop);
        imageViewIcon = (ImageView) findViewById(R.id.imageViewIcon);

        textViewTitle = (TextView) findViewById(R.id.textViewTitle);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnJoinCommunity.setOnClickListener(this);
        btnCreateCommunity.setOnClickListener(this);

        checkMobileNumber();

    }

    private void checkMobileNumber() {

        final ProgressDialog progDial= ProgressDialog.show(CreateJoinActivity.this, "", "Please wait...", true);

        ParseQuery<ParseObject> queryUserAccount = new ParseQuery<ParseObject>(
                "Member");
        queryUserAccount.whereEqualTo("mobileNumber",
                PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.mobileNumber, ""));

        queryUserAccount.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                 try {
                     if(parseObject!=null) {
                         String memberCommunityName = parseObject.getString("communityName");
                         String memberCommunityType = parseObject.getString("communityType");
                         String memberCommunityVPA = parseObject.getString("communityVPA");
                         String adminMobileNumber = parseObject.getString("adminMobileNumber");


                         SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(CreateJoinActivity.this);
                         SharedPreferences.Editor editor = sharedPreferences.edit();
                         editor.putString("name", memberCommunityName.toString());
                         editor.putString("communityVPA", memberCommunityVPA.toString());
                         editor.putString("adminMobile", adminMobileNumber);

                         editor.putString("communityType", memberCommunityType.toString());
                         editor.putString("communityName", memberCommunityName.toString());
                         editor.commit();

//                         progressBar.setVisibility(View.GONE);
                         progDial.dismiss();
                         LinearLayoutTop.setVisibility(View.VISIBLE);
                         imageViewIcon.setVisibility(View.VISIBLE);
                         if (e == null) {
                             textViewTitle.setText("You have 1 Community request");
                             btnCreateCommunity.setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
                             btnJoinCommunity.setBackgroundColor(getResources().getColor(R.color.colorSkyblue));
                             btnCreateCommunity.setEnabled(false);
                         } else {
                             btnJoinCommunity.setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
                             btnCreateCommunity.setBackgroundColor(getResources().getColor(R.color.colorSkyblue));
                             btnJoinCommunity.setEnabled(false);
                         }
                     }else{
//                         progressBar.setVisibility(View.GONE);
                         progDial.dismiss();
                         LinearLayoutTop.setVisibility(View.VISIBLE);
                         imageViewIcon.setVisibility(View.VISIBLE);
                         btnJoinCommunity.setBackgroundColor(getResources().getColor(R.color.colorLightGrey));
                         btnCreateCommunity.setBackgroundColor(getResources().getColor(R.color.colorSkyblue));
                         btnJoinCommunity.setEnabled(false);
                     }
                 }catch(Exception e1){
                     e1.printStackTrace();
                 }
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnJoinCommunity) {
            startActivity(new Intent(CreateJoinActivity.this, JoinCommunityActivity.class));

        } else if (view.getId() == R.id.btnCreateCommunity) {

            startActivity(new Intent(CreateJoinActivity.this, ActivityCreateCommunity.class));
        }
    }

}
