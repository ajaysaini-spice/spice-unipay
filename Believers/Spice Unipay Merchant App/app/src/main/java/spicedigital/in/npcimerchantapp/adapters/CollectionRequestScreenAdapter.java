package spicedigital.in.npcimerchantapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.database.MyDataBase;
import spicedigital.in.npcimerchantapp.home_screen.PaymentRequestforMembers;

/**
 * Created by CH-E01062 on 20-03-2016.
 */
public class CollectionRequestScreenAdapter extends BaseAdapter {


    Context mContext;
    ArrayList<HashMap<String, String>> mList;

    public CollectionRequestScreenAdapter(Context context,ArrayList<HashMap<String, String>> list) {

        mList = list;
        mContext = context;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.collection_request_item, null);
            holder = new ViewHolder();
            holder.fromDate = (TextView) convertView.findViewById(R.id.from_date);
            holder.payLater = (TextView) convertView.findViewById(R.id.pay_later);

            holder.payNow = (TextView) convertView.findViewById(R.id.pay_now);
            holder.amount = (TextView) convertView.findViewById(R.id.amount);
            holder.eventNmae = (TextView) convertView.findViewById(R.id.event_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        HashMap<String, String> recordData = mList.get(position);

        holder.eventNmae.setText(recordData.get(MyDataBase.KEY_EVENT_NAME));

        holder.fromDate.setText(recordData.get(MyDataBase.KEY_EVENT_TIMESTAMP));
        holder.amount.setText(mContext.getResources().getString(R.string.rupees)+" "+recordData.get(MyDataBase.KEY_AMOUNT));
        holder.payNow.setTag(position);

        holder.payNow.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                try {
                    int position= Integer.parseInt(String.valueOf(arg0.getTag()));

                    HashMap<String, String> dataSelected = mList.get(position);

                    PaymentRequestforMembers paymentRequestforMembers = (PaymentRequestforMembers) mContext;

                    paymentRequestforMembers.callPayRequest(dataSelected);


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }
        });

        return convertView;
    }

    static class ViewHolder {
        private TextView fromDate, payLater, payNow, amount, eventNmae;

    }
}
