package spicedigital.in.npcimerchantapp.parse;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import spicedigital.in.npcimerchantapp.R;
import spicedigital.in.npcimerchantapp.application.AppControler;
import spicedigital.in.npcimerchantapp.home_screen.PaymentRequestforMembers;
import spicedigital.in.npcimerchantapp.home_screen.PaymentRequestiesActivity;

/**
 * Created by CH-E01006 on 3/19/2016.
 */
public class ParseReceiver extends ParsePushBroadcastReceiver {
    private java.lang.String PARSE_DATA_KEY = "com.parse.Data";
    private int NOTIFICATION_ID = 1;
    private String title = "", message = "",PersonType="";

    @Override
    protected Notification getNotification(Context context, Intent intent) {
        // deactivate standard notification
        return null;
    }


    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        JSONObject data = getDataFromIntent(intent);
        parseData(context,data);

        /*NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle("Title");
        builder.setContentText("Text");
        builder.setSmallIcon(R.drawable.noticeboard);
        builder.setAutoCancel(true);


        notificationManager.notify("MyTag", 0, builder.build());*/
    }

    public void sendNotification(Context context) {
        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.app_icon);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        // RemoteViews contentView = new RemoteViews(getPackageName(),
        // R.layout.custom_notification);
        // contentView.setImageViewResource(R.id.iv_notification,
        // R.drawable.logo);
        // contentView.setTextViewText(R.id.tv_message, msg);
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        PersonType = sharedPreferences.getString("PersonType","");
        PendingIntent contentIntent;
        if(PersonType.equalsIgnoreCase("ADMIN")){
             contentIntent = PendingIntent
                    .getActivity(
                            context,
                            (int) System.currentTimeMillis(),
                            new Intent(context, PaymentRequestiesActivity.class)
                                    .setFlags(
                                            Intent.FLAG_ACTIVITY_CLEAR_TOP),

                            0);
        }else{
             contentIntent = PendingIntent
                    .getActivity(
                            context,
                            (int) System.currentTimeMillis(),
                            new Intent(context, PaymentRequestforMembers.class)
                                    .setFlags(
                                            Intent.FLAG_ACTIVITY_CLEAR_TOP),

                            0);
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                context)
                .setSmallIcon(R.drawable.notification_icon)
                .setLargeIcon(largeIcon)
                .setTicker("")
                .setContentTitle(title)
                .setLargeIcon(largeIcon)
                .setContentText(message)
                .setStyle(
                        new NotificationCompat.BigTextStyle().bigText(message))
                .setDefaults(-1);

        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void parseData(Context context,JSONObject data) {
        try {
            JSONObject alertObj = data.optJSONObject("alert");
            if (alertObj != null) {
                String amount = alertObj.optString("amount");
                String endDate = alertObj.optString("endDate");
                String eventDesc = alertObj.optString("eventDesc");
                String eventName = alertObj.optString("eventName");
                String eventType = alertObj.optString("eventType");
                String payeeAddress = alertObj.optString("payeeAddress");
                String payeeName = alertObj.optString("payeeName");
                String payeemobileNumber = alertObj.optString("payeemobileNumber");
                String startDate = alertObj.optString("startDate");
                String timeStamp = alertObj.optString("timeStamp");
                String ownName= alertObj.optString("ownName");
                title = alertObj.optString("title");
                message = alertObj.optString("message");
                String walletId = alertObj.optString("walletId");
                sendNotification(context);

                AppControler.getDataBaseInstance().addNotification(timeStamp, walletId, eventName, eventType, startDate, "", "", payeeName, payeeAddress, amount, message, timeStamp, "Pending", "100", "200", ownName, "", "");
            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        //   super.onPushOpen(context, intent);
    }

    private JSONObject getDataFromIntent(Intent intent) {
        JSONObject data = null;
        try {
            data = new JSONObject(intent.getExtras().getString(PARSE_DATA_KEY));
        } catch (JSONException e) {
            // Json was not readable...
        }
        return data;
    }
}

