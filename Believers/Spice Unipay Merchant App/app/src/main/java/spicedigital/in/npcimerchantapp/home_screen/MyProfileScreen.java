package spicedigital.in.npcimerchantapp.home_screen;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import spicedigital.in.npcimerchantapp.R;

/**
 * Created by ch-e00812 on 3/19/2016.
 */
public class MyProfileScreen extends AppCompatActivity {


    Toolbar toolbar;
    View view;
    TextView titleText;
    SharedPreferences pref;
    TextView nameTxt, dobTxt, genderTxt, vpaTxt, accTxt, mmidTxt, ifscTxt, aadharCardTxt;

    Button editBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile_screen);

        initialiseToolbar();
        initialiseviews();
    }


    public void initialiseToolbar() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        view = toolbar.getRootView();
    }

    public void initialiseviews() {

        titleText = (TextView) view.findViewById(R.id.title_text);
        titleText.setText("MY PROFILE");

        pref = PreferenceManager.getDefaultSharedPreferences(this);

        nameTxt = (TextView) findViewById(R.id.name_txt);
        nameTxt.setText(pref.getString("name",""));

        dobTxt = (TextView) findViewById(R.id.dob_txt);

        genderTxt = (TextView) findViewById(R.id.gender_txt);
        vpaTxt = (TextView) findViewById(R.id.vpa_txt);


        vpaTxt.setText(pref.getString("communityVPA",""));

        accTxt = (TextView) findViewById(R.id.acc_txt);
        mmidTxt = (TextView) findViewById(R.id.mmid_txt);
        ifscTxt = (TextView) findViewById(R.id.ifsc_txt);
        aadharCardTxt = (TextView) findViewById(R.id.aadhar_card_txt);
        editBtn = (Button) findViewById(R.id.edit);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
