package spicedigital.in.npcipspserver.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by CH-E01073 on 02-03-2016.
 */

public class LatoRegularEditText extends EditText {

    public LatoRegularEditText(Context context) {
        super(context);
        style(context);
    }

    public LatoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public LatoRegularEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        try {
            Typeface tf = Typeface.createFromAsset(context.getAssets(),
                    "fonts/Lato-Regular.ttf");
            setTypeface(tf);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
