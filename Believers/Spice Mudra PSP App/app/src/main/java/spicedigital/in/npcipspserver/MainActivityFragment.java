package spicedigital.in.npcipspserver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AUTH;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;
import org.npci.upi.security.services.CLServices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private final String keyCode="NPCI";
    Button button;
    TextView textResult;
    EditText transactionID;
    EditText transactionAmount;
    Spinner spinner;
    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    ArrayList<String> listItems=new ArrayList<String>();
    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    ArrayAdapter<String> adapter;
    private ListView responseListView;
    private String xmlPayloadString="";
    private String credAllowedString = "";
    View rootView;
    private CLServices clServices;
    String token="";
    public MainActivityFragment() {
        //sendPostRequest(keyCode);
    }

    private void sendPostRequest(String ownerCode) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String ownerCode = params[0];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://172.18.17.51:9004/upi/requestListKeys");

                try {

                    String xmlRequestString="<upi:ReqListKeys xmlns:upi=\"http://npci.org/upi/schema/1/0/\"> \n" +
                            "<Head ver=\"1.0\" ts=\"2015-01-16T14:15:43+05:30\" orgId=\""+ownerCode+"\" msgId=\"1\"/>\n" +
                            "</upi:ReqListKeys>";


                    StringEntity se = new StringEntity( xmlRequestString, HTTP.UTF_8);
                    se.setContentType("text/xml");
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }

                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("Protocol HttpResponese :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("IO Exception HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                    xmlPayloadString=result;
                    Toast.makeText(getActivity().getApplicationContext(), "ListKeys XML Loaded Successfully...", Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(ownerCode);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

         rootView = inflater.inflate(R.layout.fragment_main, container, false);
        Context mContext=getActivity();
        /* Calling app_init() for declaring the event and initialization */
       // sendPostRequest(keyCode);
        /**
         * Initiate Common Library Services before using. If the service is successfully bound, serviceConnected
         * method of the ServiceConnectionStatusNotifier will be called.
         */
       /* CLServices.initService(mContext, new ServiceConnectionStatusNotifier() {
            @Override
            public void serviceConnected(CLServices services) {
                clServices = services;

                 app_init(rootView);

            }

            @Override
            public void serviceDisconnected() {
                Log.d("CL Service", "Service disconnected");
                clServices = null;
            }
        });*/
       app_init(rootView);
        return rootView;

    }

    public void app_init(View container){

        // Creating API Responses

        /* This will be obtained from the response of ListKeys API */
        /*xmlPayloadString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ns2:RespListKeys xmlns:ns2=\"http://npci.org/upi/schema/\"><Head msgId=\"5skv5ptRj1ucFTehlU7\" orgId=\"NPCI\" ts=\"2015-12-21T16:18:24+05:30\" ver=\"1.0\"/><Resp reqMsgId=\"d0kSkoT7quU6DDYk\" result=\"SUCCESS\"/><Txn id=\"d0kSkoT7quU7RaAV\" ts=\"2015-12-21T16:18:13+05:30\"/><keyList><key code=\"NPCI\" ki=\"20150822\" owner=\"NPCI\" type=\"PKI\"><keyValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAzPA1EH+aCZv/rly4zgRtfgRNr+6Xtp4iTWHEA36WaSia55gyrOAT0UJOtX9nG/NZ77wFzUxhrHuczh3lVO8/ylXh1wpcRsBPLNfg1qXzaU8c7JLk7amD4cV4re1LkqZfOOrri21na9p7Ybw8v9mC/q7xfF3gzySczaq8SG1NCQIDAQAB</keyValue></key></keyList><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>fXZgHY4Zkf0WD1dtzfR1x40lcre2aV3m83+96s5e4Fw=</DigestValue></Reference></SignedInfo><SignatureValue>OF46FU9WLGKCWCUQwt0mKWBOt0diHvtkH2puvTjU3usgCc4dOYMtCQp5neE3wnM+e3UE/kwMvpSN\n"+
                "A8WFwYxr/geQW9BcL74cG1mXiI7qTL+jf6nk6lXpbJPyL8cd0o/Lyp6ckMPWnNOrdf4rAqAjKLTw\n"+
                //"A9WFwYxr/geQW9BcL74cG1nXiI7qTL+jf6nk6lXpbJPyL8cd0o/Lyp6ckMPWnNOrdf4rAqAjKLTw\n"+
                "o09kS2AHo3QxDpFwCBfoeNpxmmnfCIK+OeoEFUGj2IUDsjaF+3DNUg3B4it72B5Auu/uFkS7OIra\n"+
                "enJzrUacNy1aiw+OmMp4W7WyqPMFPYGBRd9Sa5rlytDB7D7Ku9mUEp4XpnvyNCZxRfyCqPEDcwW3\n"+
                "nuB6U+/10GswhKJRn6L+oXWuNFDHS/BsmD5JBQ==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>01DqzBsJTyMHT2S9MK5AIyFXNU646kwiOK3uymXIy9EW0nRKNKRkeIRTlGwX4wEnymGtGgX5B/Ij\n"+
                "1elkLN4VJ9GplDV+wf0Lp2i2q4E6uRiWIzsqq42MCQgv8Fq/IPqjqPbeP9yh/8YPmBiMehBmhQd3\n"+
                "qzl77C03k6d0yBIO5q/zXneTK9uFBNEL5yNpukrLGBcf3b9VHsjXpEaQrxGSMHCgNWpQgXpEcBr5\n"+
                "OJ0/XxWbgMCZMlkYe1d6gswjuCRZ/xxJwEfbSO5AsnPtyqxSIjyhgEi9REtYnzaWwOBN4JCqt0pM\n"+
                "L0ja23lUwVJuNwkwNGKBXvkGoXUln8Sf7PIv7w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></ns2:RespListKeys>";
        Log.i("xml_payload",xmlPayloadString);*/
        xmlPayloadString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ns2:RespListKeys xmlns:ns2=\"http://npci.org/upi/schema/\"><Head msgId=\"5skv5ptRj1uBlSYA8Ny\" orgId=\"NPCI\" ts=\"2015-12-31T17:42:26+05:30\" ver=\"1.0\"/><Resp reqMsgId=\"d0kSkoT7PaUSbv0u\" result=\"SUCCESS\"/><Txn id=\"d0kSkoT7PaUStXEf\" ts=\"2015-12-31T17:42:23+05:30\"/><keyList><key code=\"NPCI\" ki=\"20150822\" owner=\"NPCI\" type=\"PKI\"><keyValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rIIEHkJ2TYgO/JUJQI/sxDgbDEAIuy9uTf4DItWeIMsG9AuilOj9R+dwAv8S6/9No/z0cwsw4UnsHQG1ALVIxFznLizMjaVJ7TJ+yTS9C9bYEFakRqH8b4jje7SC7rZ9/DtZGsaWaCaDTyuZ9dMHrgcmJjeklRKxl4YVmQJpzYLrK4zOpyY+lNPBqs+aiwJa53ZogcUGBhx/nIXfDDvVOtKzNb/08U7dZuXoiY0/McQ7xEiFcEtMpEJw5EB4o3RhE9j/IQOvc7l/BfD85+YQ5rJGk4HUb6GrQXHzfHvIOf53l1Yb0IX4v9q7HiAyOdggO+PVzXMSbrcFBrEjGZD7QIDAQAB</keyValue></key></keyList><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>GYiC65ThmzF0ka0DrD7/lLJuKcgTweu/DGNgp+n0JQE=</DigestValue></Reference></SignedInfo><SignatureValue>GjGo2YkxpUAOj+RLU3RUCLDhsPzaqHDEiUEOELjj0XYu9AuQEBEAvr6phgulwIi7zkmsrK/In47Y\n" +
                "QqsVhn9MVJZM/gr8Uym7EtgJzvx8M8TsecKoRf8NNkex8fYpBmX728kuQ73RPpbinVhn+zlqDl55\n" +
                "adDBevsucJNFkr0/fWDUX21coYYqZs5O/0OdHXBG+a/K5ruZ4DIsVaxYfbOMmZzVBjKHX0I88gqw\n" +
                "zHpiIBonax2YEhOaoDF9NaKcwKqt+xNdOJZbSj6gCE0+tKUfcpY15Xv40jheoEXiNl8xJvjX8wkd\n" +
                "SrIpNZFETpCpxnLl1dcBZFUrF+ZCZAyohmdGBw==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>01DqzBsJTyMHT2S9MK5AIyFXNU646kwiOK3uymXIy9EW0nRKNKRkeIRTlGwX4wEnymGtGgX5B/Ij\n" +
                "1elkLN4VJ9GplDV+wf0Lp2i2q4E6uRiWIzsqq42MCQgv8Fq/IPqjqPbeP9yh/8YPmBiMehBmhQd3\n" +
                "qzl77C03k6d0yBIO5q/zXneTK9uFBNEL5yNpukrLGBcf3b9VHsjXpEaQrxGSMHCgNWpQgXpEcBr5\n" +
                "OJ0/XxWbgMCZMlkYe1d6gswjuCRZ/xxJwEfbSO5AsnPtyqxSIjyhgEi9REtYnzaWwOBN4JCqt0pM\n" +
                "L0ja23lUwVJuNwkwNGKBXvkGoXUln8Sf7PIv7w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></ns2:RespListKeys>";

        /* This will be obtained from the response of ListAccountResponse API */
        credAllowedString = "{\n" +
                "\t\"CredAllowed\": [{\n" +
                "\t\t\"type\": \"PIN\",\n" +
                "\t\t\"subtype\": \"MPIN\",\n" +
                "\t\t\"dType\": \"ALPH | NUM\",\n" +
                "\t\t\"dLength\": 6\n" +
                //"\t}, {\n" +
                //"\t\t\"type\": \"OTP\",\n" +
                //"\t\t\"subtype\": \"OTP\",\n" +
                //"\t\t\"dType\": \"NUM\",\n" +
                //"\t\t\"dLength\": 6\n" +
                "\t}]\n" +
                "}";



        //textResult = (TextView) container.findViewById(R.id.textView2);
        transactionID = (EditText) container.findViewById(R.id.editTransactionID);
        //Set a dynamic Transaction ID
        transactionID.setText(String.valueOf(new Random().nextInt(99999999)));

        transactionAmount =(EditText) container.findViewById(R.id.editTransactionAmount);

        // For response view
        responseListView =(ListView) container.findViewById(R.id.listView);
     //   spinner = (Spinner) container.findViewById(R.id.controls_spinner);
        adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listItems);
        responseListView.setAdapter(adapter);

       /* ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.controls_arrays, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        Log.i("Spinner", spinner.getSelectedItem().toString());*/

        //Pay button
      //  new Loading_fail().execute();

        button = (Button) container.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent intent = new Intent(getActivity(), PinActivityComponent.class);

                // Create Keycode
                intent.putExtra("keyCode", keyCode);

                // Create xml payload
                if(xmlPayloadString.isEmpty()){
                    Toast.makeText(getActivity(),"XML List Key API is not loaded.",Toast.LENGTH_LONG).show();
                    return;
                }
                intent.putExtra("keyXmlPayload", xmlPayloadString);  // It will get the data from list keys API response

                // Create Controls
                if(credAllowedString.isEmpty()){
                    Toast.makeText(getActivity(),"Required Credentials could not be loaded.",Toast.LENGTH_LONG).show();
                    return;
                }
               intent.putExtra("controls", credAllowedString);

                // Create Configuration
                JSONObject configuration = new JSONObject();
                try {

                    configuration.put("payerBankName", "Indian Bank Ltd.");
                    configuration.put("backgroundColor","#FFFFFF");
                    Log.i("configuration",configuration.toString());
                   intent.putExtra("configuration", configuration.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Create Salt
                JSONObject salt = new JSONObject();
                try {

                  //  salt.put("txnId", transactionID.getText().toString());
                    salt.put("txnId","11618");
                    salt.put("txnAmount", "200");
                    salt.put("deviceId", "74235ae00124fab8");
                    salt.put("appId", getActivity().getApplicationContext().getPackageName());
                    Log.i("salt", salt.toString());
                    intent.putExtra("salt", salt.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Create Pay Info
                JSONArray payInfoArray=new JSONArray();
                try {
                    JSONObject jsonPayeeName = new JSONObject();
                    jsonPayeeName.put("name", "payeeName");
                    jsonPayeeName.put("value", "John Smith");
                    payInfoArray.put(jsonPayeeName);

                    JSONObject jsonNote = new JSONObject();
                    jsonNote.put("name", "note");
                    jsonNote.put("value", "Pay restaurant bill");
                    payInfoArray.put(jsonNote);

                    JSONObject jsonRefId = new JSONObject();
                    jsonRefId.put("name", "refId");
                   // jsonRefId.put("value", transactionID.getText().toString());
                    jsonRefId.put("value", "11618");

                    payInfoArray.put(jsonRefId);

                    JSONObject jsonRefUrl = new JSONObject();
                    jsonRefUrl.put("name", "refUrl");
                    jsonRefUrl.put("value", "https://indianbank.com");
                    payInfoArray.put(jsonRefUrl);

                    JSONObject jsonAccount = new JSONObject();
                    jsonAccount.put("name", "account");
                    jsonAccount.put("value", "122XXX423");
                    payInfoArray.put(jsonAccount);

                    Log.i("payInfo", payInfoArray.toString());
                    intent.putExtra("payInfo", payInfoArray.toString());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

               /* String trustStr = null;
                try {
                    StringBuilder trustParamBuilder = new StringBuilder(100);
                    trustParamBuilder.append(transactionAmount.getText()
                            .toString()).append(CLConstants.SALT_DELIMETER)
                            .append(transactionID.getText().toString()).append(CLConstants.SALT_DELIMETER)
                            .append("zeeshan.khan@sbi").append(CLConstants.SALT_DELIMETER)
                            .append("rohit.patekar@hdfc").append(CLConstants.SALT_DELIMETER)
                            .append(CommonUtils.getAppId(getActivity())).append(CLConstants.SALT_DELIMETER)
                            .append(CommonUtils.getMobileNumber()).append(CLConstants.SALT_DELIMETER)
                            .append(CommonUtils.getDeviceId(getActivity()));
                    //trustStr = TrustCreator.createTrust(trustParamBuilder.toString(), token);
                    trustStr = trustParamBuilder.toString();
                    //intent.putExtra("trust", trustStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                // Create Language Pref
                intent.putExtra("languagePref", "en_US");

               startActivityForResult(intent, 1);

                /**
                 * create a CLRemoteResultReceiver instance to receive the encrypted credential.
                 *//*
                CLRemoteResultReceiver remoteResultReceiver = new CLRemoteResultReceiver(new ResultReceiver(new Handler()) {
                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        super.onReceiveResult(resultCode, resultData);
                        parseResult(resultData);
                    }
                });

                *//**
                 * Call get Credential
                 *//*
                clServices.getCredential(keyCode, xmlPayloadString, credAllowedString,
                        configuration.toString(), salt.toString(), payInfoArray.toString(), trustStr, "en_US", remoteResultReceiver);*/
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
       if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
           String errorMsgStr = data.getStringExtra("error");
           if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
               Log.d("Error:", errorMsgStr);
               try {
                   JSONObject error = new JSONObject(errorMsgStr);
                   String errorCode = error.getString("errorCode");
                   String errorText = error.getString("errorText");
                   Toast.makeText(getActivity().getApplicationContext(), errorCode + ":" + errorText, Toast.LENGTH_LONG).show();
               } catch (JSONException e) {
                   e.printStackTrace();
               }
               return;
           }
           HashMap<String, String> credListHashMap = (HashMap<String, String>)data.getSerializableExtra("credBlocks");
           for(String cred : credListHashMap.keySet()){ // This will return the list of field name e.g mpin,otp etc...
               try {
                   JSONObject credBlock=new JSONObject(credListHashMap.get(cred));
                   Log.i("enc_msg", credBlock.toString());
                   token=credBlock.getJSONObject("data").getString("encryptedBase64String");
                   adapter.add("Control name : "+cred);
                   adapter.add("keyId : " + credBlock.getJSONObject("data").getString("code"));
                   adapter.add("key index : " + credBlock.getJSONObject("data").getString("ki"));
                   adapter.add("Encrypted Message from Common Library: " + credBlock.getJSONObject("data").getString("encryptedBase64String"));

                   Log.i("enc_msg", credBlock.getJSONObject("data").getString("encryptedBase64String"));
                   //Log.i("enc_msg", credBlock.getString("message"));
                  // new Loading_fail().execute();
               } catch (JSONException e) {
                   e.printStackTrace();
               }

           }
        }
    }
    class C02431 implements HostnameVerifier {
        C02431() {
        }

        public boolean verify(String hostname, SSLSession sslSession) {
            return true;
        }
    }

    class C04262 implements X509HostnameVerifier {
        C04262() {
        }

        public boolean verify(String s, SSLSession sslSession) {
            return true;
        }

        public void verify(String s, SSLSocket sslSocket) throws IOException {
        }

        public void verify(String s, X509Certificate x509Certificate) throws SSLException {
        }

        public void verify(String s, String[] strings, String[] strings2) throws SSLException {
        }
    }

    //private String URL =  "http://103.14.161.149:12001/UpiService/upi/collectTransService";
    private String URL = "http://103.14.161.149:12001/UpiService/upi/payTransService";

    class Loading_fail extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            callHTTPService();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public String callHTTPService() {


        String json = "";
        JSONObject payload = null;

        try {

            payload = new JSONObject();
            // OTPService
           /* payload.put("txnId", "12332423");
            payload.put("payerAddress", "nikhil@mgat");
            payload.put("payerName", "Nikhil M");
            payload.put("mobileNumber", "8654589562");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Mumbai,Maharashtra ");
            payload.put("ip", "142.12.26.52");
            payload.put("type", "mob");
            payload.put("id", "154fer53dfdf");
            payload.put("os", "android");

            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("accountAddressType", "ACCOUNT");*/

           /* JSONObject payload2 = new JSONObject();
            payload2.put("txnId", String.valueOf("84895995"));
            payload2.put("payerAddress", "nikhil@mgat");
            payload2.put("payerName", "vdnvkfl");
            payload2.put("mobileNumber", "9888762945");
            payload2.put("geoCode", "123466454 ");
            payload2.put("location", "Mumbai,Maharashtra ");
            payload2.put("ip", "142.12.26.52");
            payload2.put("type", "mob");
            payload2.put("id", "154fer53dfdf");
            payload2.put("os", "android");
            payload2.put("app", "MAPP_PSP");
            payload2.put("capability", "453453d4f5343434df354");
            payload2.put("accountAddressType", "ACCOUNT");
            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000106");
            arrabj.put("acType", BuildConfig.FLAVOR);
            arrabj.put("acNum", BuildConfig.FLAVOR);
            arrabj.put("iin", BuildConfig.FLAVOR);
            arrabj.put("uIdNum", BuildConfig.FLAVOR);
            arrabj.put("mmId", BuildConfig.FLAVOR);
            arrabj.put("mobNum", BuildConfig.FLAVOR);
            arrabj.put("cardNum", BuildConfig.FLAVOR);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload2.put("detailsJson", jsonArray);
            Log.d("@OTP_Input", payload2.toString());
            payload = payload2;*/




           // payload.put("txnId", transactionID.getText().toString());
            payload.put("txnId","456789047");
            payload.put("txnNote", "Bill Payment");
            payload.put("payerAddress", "zeeshan.khan@mapp");

            payload.put("payerName", "Zeeshan khan");
            //  payload.put("mobileNumber", "8654589562");
            payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "142.12.26.52");
            payload.put("type", "mob");
            payload.put("id", "154fer53dfdf");
            //payload.put("id", "799991");
            payload.put("os", "android");

            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("payerAcAddressType", "ACCOUNT");

            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000103");
            arrabj.put("acType", "SAVINGS");
            arrabj.put("acNum", "45151545548799");
            arrabj.put("iin", "");
            arrabj.put("uIdNum", "");
            arrabj.put("mmId", "");
            arrabj.put("mobNum", "");
            arrabj.put("cardNum", "");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload.put("detailsJson", jsonArray);

            payload.put("credType", "PIN");
          payload.put("credSubType", "MPIN");
           /* payload.put("credDataValue", "1.0|K8zWQGH/eJYHAVwW6gu7e2XXIgeWvUG6KQQ5HxWCt/mq9eSfMtJCsIRpn0g0I1l5EJE2OrFhfacNI9zN7jgSSOElKFNwUeWwt1iKssVLxXWSXtNkMZYsXBQXziyacZJ5azxV6R54ShFBeGmT/5Gku0bHPbU7kPwSCF9zd3ffehFx95XCXgqNfrUHePyBVjWBhqVR5mQZVUQS+ED6beB2KENUlatOZlALcBRtqTpE9kZ8WYhr4Ejk9ojdmgjJuZpYr4ELwlZFi+HgZKZSNr9ayaCCafgHijgByvIob+gU4FAv8ef8JPfv0bVyDJi1eUzW50OQ2EFKeg8npynmx+0Jg==");
*/
           /* payload.put("credDataValue", "1.0|hf2CE5Zy/27d/YVtKx7xqWusqJ/40Nsjj3wlVKbOXm1UhlQFo9XkIh8vOVMI/WK3V3ywPr1Bdqu2V/S8h20ZGb1UQHq9wcuJ0xhCX1M0u6FHEo37+8EbawRVmD2U2B2UJ7X0FLblfm4mD3hSv9sCm9AtdqsC2VFcWQMl4VeJ9SKM5qc/Q3M9xPA3kbrkNelYZXBnkxOBJh6xgwv99jsP7u9BiFt8NEGF3NkE0d6Ae9ON/c6y6ajggFMKe6tqXO3aRh5vnXMYeg40IS34eY56QJ99vQAVaHu4Rwu1i2oHoSuUcob8HFqkqsNDT6+IOVqgEyYo8yh7xMPuHbIGjyc+Vw==");
*/

            payload.put("credDataValue", token);
            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");
            payload.put("txnAmmount", "200");
            payload.put("payeeAddress", "rohit.patkar@mapp");
            payload.put("payeeName", "Aditya Bera");

            payload.put("payeeType", "PERSON");
            payload.put("payeeCode", "0000");
            payload.put("IdentityId", "45785454");
            payload.put("custRefId", "FG4563322");
            payload.put("refId", "A45586699633324");
            //  payload.put("txnType", "LIST_KEYS");

           /* SSLContext sc = null;
            try {
                sc = SSLContext.getInstance(SSLSocketFactory.TLS);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new C02391());*/

            SSLContext sc;
            HttpClient httpClient;
            HttpPost httpPost;
            BufferedReader reader;
            StringBuilder sb;
            String line;
            InputStream is;


            sc = SSLContext.getInstance(SSLSocketFactory.TLS);
            sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new C02431());
            httpClient = new DefaultHttpClient();
            ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04262());
            httpPost = new HttpPost(URL);
            httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
            httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
            httpPost.setHeader("Content-type", "application/json");
            is = httpClient.execute(httpPost).getEntity().getContent();
            reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            sb = new StringBuilder();
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("@GetAccountResponse", json);
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
        } catch (ClientProtocolException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        } catch (NoSuchAlgorithmException e7) {
            e7.printStackTrace();
        } catch (KeyManagementException e8) {
            e8.printStackTrace();
        } catch (JSONException e) {
        }


          /*  HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity((new StringEntity(payload.toString(), "UTF-8")));
            httpPost.setHeader("Authorization",
                    "Basic bWdzdXBpOmFkbWluQDEyMw=="); // +
            // LoginActivity.token.getToken());
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("@Response", json);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

        String response = json.toString();

        return response;

    }

    private void parseResult(Bundle data) {
        String errorMsgStr = data.getString("error");
        if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
            Log.d("Error:", errorMsgStr);
            try {
                JSONObject error = new JSONObject(errorMsgStr);
                String errorCode = error.getString("errorCode");
                String errorText = error.getString("errorText");
                Toast.makeText(getActivity().getApplicationContext(),
                        errorCode + ":" + errorText, Toast.LENGTH_LONG)
                        .show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return;
        }
        HashMap<String, String> credListHashMap = (HashMap<String, String>) data
                .getSerializable("credBlocks");
        for (String cred : credListHashMap.keySet()) {
            // This will return the list of field name e.g mpin,otp etc...
            try {
                JSONObject credBlock = new JSONObject(
                        credListHashMap.get(cred));
                Log.i("enc_msg", credBlock.toString());
                adapter.add("Control name : " + cred);
                adapter.add("keyId : "
                        + credBlock.getJSONObject("data").getString("code"));
                adapter.add("key index : "
                        + credBlock.getJSONObject("data").getString("ki"));
                adapter.add("Encrypted Message from Common Library: "
                        + credBlock.getJSONObject("data").getString(
                        "encryptedBase64String"));

                Log.i("enc_msg",
                        credBlock.getJSONObject("data").getString(
                                "encryptedBase64String"));
                // Log.i("enc_msg", credBlock.getString("message"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
