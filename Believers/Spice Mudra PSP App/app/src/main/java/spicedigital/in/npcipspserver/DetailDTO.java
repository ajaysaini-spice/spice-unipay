package spicedigital.in.npcipspserver;

/**
 * Created by CH-E01006 on 3/16/2016.
 */
public class DetailDTO {
    String accno;
    String ifsccode;
    String mobno;
    String payeeaddr;
    String payeename;
    String txnId;

    public String getPayeename() {
        return this.payeename;
    }

    public void setPayeename(String payeename) {
        this.payeename = payeename;
    }

    public String getPayeeaddr() {
        return this.payeeaddr;
    }

    public void setPayeeaddr(String payeeaddr) {
        this.payeeaddr = payeeaddr;
    }

    public String getIfsccode() {
        return this.ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }

    public String getAccno() {
        return this.accno;
    }

    public void setAccno(String accno) {
        this.accno = accno;
    }

    public String getMobno() {
        return this.mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getTxnId() {
        return this.txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }
}
