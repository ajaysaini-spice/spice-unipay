package spicedigital.in.npcipspserver;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.auth.AUTH;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.PinActivityComponent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;


public class FirstActivity extends AppCompatActivity {
    private final String keyCode="NPCI";
    private String xmlPayloadString="";
    private String credAllowedString = "";
    String transactionID="";
    String transactionAmount="";
    RelativeLayout rl_transaction,rl_layout;
    LinearLayout ll_bottom;
    String token="";
    String TYPE="";
    String REQUEST="";
    Boolean  requestcheck=false;
    TextView payer_name,pva,mb_num;
    AutoCompleteTextView bene_acc_no;
    TextView payee_name,amount,oderid,btn_submit,btn_cancel,txt_transid,txt_upperlabel;
    ImageView imgs;
    String IFSC="";
    String payerName="",payerVpa="",mobileNumber="",accNum="",payeeName="",payeeVpa="";
  String[] accounts;
    Dialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        CommonUtils.setStatusBarColor(FirstActivity.this);
        try {
            intialize();
            processRequestIntent(getIntent());
        } catch (Exception e) {
        }


    }
    public void intialize(){
        try {
            transactionID = String.valueOf(new Random().nextInt(99999999));
            txt_upperlabel = (TextView) findViewById(R.id.txt_upperlabel);
            imgs = (ImageView) findViewById(R.id.imgs);
            txt_transid = (TextView) findViewById(R.id.txt_transid);
            rl_transaction = (RelativeLayout) findViewById(R.id.rl_transaction);
            rl_layout = (RelativeLayout) findViewById(R.id.rl_layout);
            ll_bottom = (LinearLayout) findViewById(R.id.ll_bottom);

            payer_name = (TextView) findViewById(R.id.payer_name);
            pva = (TextView) findViewById(R.id.pva);
            mb_num = (TextView) findViewById(R.id.mb_num);
            bene_acc_no = (AutoCompleteTextView) findViewById(R.id.acc_no);
            bene_acc_no.setFocusable(false);
            bene_acc_no.setClickable(true);
            //   bene_acc_no.setInputType(InputType.TYPE_NULL);

            accounts = getResources().getStringArray(
                    R.array.accounts);


            final ArrayAdapter<String> Id_Card_adapter = new ArrayAdapter<String>(
                    FirstActivity.this,
                    R.layout.custom_simple_dropdown_item, accounts);
            bene_acc_no.setAdapter(Id_Card_adapter);
            bene_acc_no.setThreshold(0);
            bene_acc_no.setText(accounts[0]);

            bene_acc_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        bene_acc_no.showDropDown();
                    }
                }
            });

            bene_acc_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bene_acc_no.showDropDown();
                }
            });
            // id_card.setOnKeyListener(this);
            // id_card.setKeyListener(null);

//        bene_acc_no.setClickable(true);
//        bene_acc_no.setEnabled(true);
//        bene_acc_no.setPressed(true);
            //    bene_acc_no.setTextIsSelectable(true);
//        bene_acc_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    bene_acc_no.setAdapter(Id_Card_adapter);
//                    bene_acc_no.setThreshold(0);
//                }
//
//            }
//        });
//        bene_acc_no.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View arg0) {
////                bene_acc_no.setAdapter(Id_Card_adapter);
////                bene_acc_no.setThreshold(0);
//            }
//        });

            payee_name = (TextView) findViewById(R.id.payee_name);
            oderid = (TextView) findViewById(R.id.oderid);
            amount = (TextView) findViewById(R.id.amount);

            btn_submit = (TextView) findViewById(R.id.btn_submit);
            btn_cancel = (TextView) findViewById(R.id.btn_cancel);

            btn_submit.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                app_init();
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

            btn_cancel.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setResult(Constants.BACK);
                            finish();
                        }
                    });

            progressBar = new Dialog(FirstActivity.this);
            progressBar.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressBar.setCancelable(false);
            progressBar.setContentView(R.layout.dialog_railloader);
            final TextView txt_upperlabel = (TextView) progressBar
                    .findViewById(R.id.txt_upperlabel);
            txt_upperlabel.setText("This may take some time depending on your connection speed");
            final ImageView imgs = (ImageView) progressBar
                    .findViewById(R.id.imgs);

            final ImageView image_railprogressdilaog = (ImageView) progressBar
                    .findViewById(R.id.image_railprogressdilaog);
            image_railprogressdilaog.setVisibility(View.INVISIBLE);
            startAnimation(imgs);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void processRequestIntent(Intent intent) {
  try {
      String status1 = BuildConfig.FLAVOR;
      Uri uri = intent.getData();
      REQUEST = uri.getQueryParameter("REQUEST");
      TYPE = uri.getQueryParameter("TYPE");

      JSONObject jsonObject = new JSONObject(REQUEST);


       payerName = jsonObject.getString("payerName");
       payerVpa = jsonObject.getString("payerAddress");
       mobileNumber = jsonObject.getString("mobileNumber");
       payeeName = jsonObject.getString("payeeName");
      payeeVpa = jsonObject.getString("payeeAddress");
      transactionAmount = jsonObject.getString("amount");

      payer_name.setText(payerName);
      pva.setText(payerVpa);
      mb_num.setText(mobileNumber);

      payee_name.setText(payeeName);
      oderid.setText(transactionID);
      amount.setText(transactionAmount);

  }catch(Exception e){
      e.printStackTrace();
  }

    }

    public void app_init(){

        // Creating API Responses
      try {
          if (bene_acc_no.getText().toString().trim().split("\\/")[1].equals("XXXXX65214")) {
              IFSC = "MAPP0000106";
              accNum = "00500745865214";
          } else {
              IFSC = "MAPP0000103";
              accNum = "00500745865210";
          }


          xmlPayloadString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><ns2:RespListKeys xmlns:ns2=\"http://npci.org/upi/schema/\"><Head msgId=\"5skv5ptRj1uBlSYA8Ny\" orgId=\"NPCI\" ts=\"2015-12-31T17:42:26+05:30\" ver=\"1.0\"/><Resp reqMsgId=\"d0kSkoT7PaUSbv0u\" result=\"SUCCESS\"/><Txn id=\"d0kSkoT7PaUStXEf\" ts=\"2015-12-31T17:42:23+05:30\"/><keyList><key code=\"NPCI\" ki=\"20150822\" owner=\"NPCI\" type=\"PKI\"><keyValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4rIIEHkJ2TYgO/JUJQI/sxDgbDEAIuy9uTf4DItWeIMsG9AuilOj9R+dwAv8S6/9No/z0cwsw4UnsHQG1ALVIxFznLizMjaVJ7TJ+yTS9C9bYEFakRqH8b4jje7SC7rZ9/DtZGsaWaCaDTyuZ9dMHrgcmJjeklRKxl4YVmQJpzYLrK4zOpyY+lNPBqs+aiwJa53ZogcUGBhx/nIXfDDvVOtKzNb/08U7dZuXoiY0/McQ7xEiFcEtMpEJw5EB4o3RhE9j/IQOvc7l/BfD85+YQ5rJGk4HUb6GrQXHzfHvIOf53l1Yb0IX4v9q7HiAyOdggO+PVzXMSbrcFBrEjGZD7QIDAQAB</keyValue></key></keyList><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>GYiC65ThmzF0ka0DrD7/lLJuKcgTweu/DGNgp+n0JQE=</DigestValue></Reference></SignedInfo><SignatureValue>GjGo2YkxpUAOj+RLU3RUCLDhsPzaqHDEiUEOELjj0XYu9AuQEBEAvr6phgulwIi7zkmsrK/In47Y\n" +
                  "QqsVhn9MVJZM/gr8Uym7EtgJzvx8M8TsecKoRf8NNkex8fYpBmX728kuQ73RPpbinVhn+zlqDl55\n" +
                  "adDBevsucJNFkr0/fWDUX21coYYqZs5O/0OdHXBG+a/K5ruZ4DIsVaxYfbOMmZzVBjKHX0I88gqw\n" +
                  "zHpiIBonax2YEhOaoDF9NaKcwKqt+xNdOJZbSj6gCE0+tKUfcpY15Xv40jheoEXiNl8xJvjX8wkd\n" +
                  "SrIpNZFETpCpxnLl1dcBZFUrF+ZCZAyohmdGBw==</SignatureValue><KeyInfo><KeyValue><RSAKeyValue><Modulus>01DqzBsJTyMHT2S9MK5AIyFXNU646kwiOK3uymXIy9EW0nRKNKRkeIRTlGwX4wEnymGtGgX5B/Ij\n" +
                  "1elkLN4VJ9GplDV+wf0Lp2i2q4E6uRiWIzsqq42MCQgv8Fq/IPqjqPbeP9yh/8YPmBiMehBmhQd3\n" +
                  "qzl77C03k6d0yBIO5q/zXneTK9uFBNEL5yNpukrLGBcf3b9VHsjXpEaQrxGSMHCgNWpQgXpEcBr5\n" +
                  "OJ0/XxWbgMCZMlkYe1d6gswjuCRZ/xxJwEfbSO5AsnPtyqxSIjyhgEi9REtYnzaWwOBN4JCqt0pM\n" +
                  "L0ja23lUwVJuNwkwNGKBXvkGoXUln8Sf7PIv7w==</Modulus><Exponent>AQAB</Exponent></RSAKeyValue></KeyValue></KeyInfo></Signature></ns2:RespListKeys>";


          credAllowedString = "{\n" +
                  "\t\"CredAllowed\": [{\n" +
                  "\t\t\"type\": \"PIN\",\n" +
                  "\t\t\"subtype\": \"MPIN\",\n" +
                  "\t\t\"dType\": \"ALPH | NUM\",\n" +
                  "\t\t\"dLength\": 6\n" +

                  "\t}]\n" +
                  "}";


          //Set a dynamic Transaction ID


          Intent intent = new Intent(FirstActivity.this, PinActivityComponent.class);

          // Create Keycode
          intent.putExtra("keyCode", keyCode);

          // Create xml payload
          if (xmlPayloadString.isEmpty()) {
              Toast.makeText(FirstActivity.this, "XML List Key API is not loaded.", Toast.LENGTH_LONG).show();
              return;
          }
          intent.putExtra("keyXmlPayload", xmlPayloadString);  // It will get the data from list keys API response

          // Create Controls
          if (credAllowedString.isEmpty()) {
              Toast.makeText(FirstActivity.this, "Required Credentials could not be loaded.", Toast.LENGTH_LONG).show();
              return;
          }
          intent.putExtra("controls", credAllowedString);

          // Create Configuration
          JSONObject configuration = new JSONObject();
          try {

              configuration.put("payerBankName", "Indian Bank Ltd.");
              configuration.put("backgroundColor", "#FFFFFF");
              Log.i("configuration", configuration.toString());
              intent.putExtra("configuration", configuration.toString());
          } catch (JSONException e) {
              e.printStackTrace();
          }

          // Create Salt
          JSONObject salt = new JSONObject();
          try {

              //  salt.put("txnId", transactionID.getText().toString());
              salt.put("txnId", transactionID);
              salt.put("txnAmount", transactionAmount);
              salt.put("deviceId", "74235ae00124fab8");
              salt.put("appId", getApplicationContext().getPackageName());
              Log.i("salt", salt.toString());
              intent.putExtra("salt", salt.toString());
          } catch (JSONException e) {
              e.printStackTrace();
          }

          // Create Pay Info
          JSONArray payInfoArray = new JSONArray();
          try {
              JSONObject jsonPayeeName = new JSONObject();
              jsonPayeeName.put("name", "payeeName");
              jsonPayeeName.put("value", payee_name);
              payInfoArray.put(jsonPayeeName);

              JSONObject jsonNote = new JSONObject();
              jsonNote.put("name", "note");
              jsonNote.put("value", "Pay restaurant bill");
              payInfoArray.put(jsonNote);

              JSONObject jsonRefId = new JSONObject();
              jsonRefId.put("name", "refId");
              jsonRefId.put("value", transactionID);

              payInfoArray.put(jsonRefId);

              JSONObject jsonRefUrl = new JSONObject();
              jsonRefUrl.put("name", "refUrl");
              jsonRefUrl.put("value", "https://indianbank.com");
              payInfoArray.put(jsonRefUrl);

              JSONObject jsonAccount = new JSONObject();
              jsonAccount.put("name", "account");
              jsonAccount.put("value", accNum);
              payInfoArray.put(jsonAccount);

              Log.i("payInfo", payInfoArray.toString());
              intent.putExtra("payInfo", payInfoArray.toString());
          } catch (Exception ex) {
              ex.printStackTrace();
          }


          // Create Language Pref
          intent.putExtra("languagePref", "en_US");

          startActivityForResult(intent, 1);
      }catch(Exception e){
          e.printStackTrace();
      }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("Result Code:", String.valueOf(resultCode));
        if (requestCode == 1 && resultCode == Activity.RESULT_OK && data != null) {
            String errorMsgStr = data.getStringExtra("error");
            if (errorMsgStr != null && !errorMsgStr.isEmpty()) {
                Log.d("Error:", errorMsgStr);
                try {
                    JSONObject error = new JSONObject(errorMsgStr);
                    String errorCode = error.getString("errorCode");
                    String errorText = error.getString("errorText");
                    Toast.makeText(FirstActivity.this, errorCode + ":" + errorText, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return;
            }
            HashMap<String, String> credListHashMap = (HashMap<String, String>)data.getSerializableExtra("credBlocks");
            for(String cred : credListHashMap.keySet()){ // This will return the list of field name e.g mpin,otp etc...
                try {
                    JSONObject credBlock=new JSONObject(credListHashMap.get(cred));
                    Log.i("enc_msg", credBlock.toString());
                    token=credBlock.getJSONObject("data").getString("encryptedBase64String");

                    token=token.replace("\n","");
                    token=token.replaceAll("'\'","");

                    SharedPreferences sharedPreferences = PreferenceManager
                            .getDefaultSharedPreferences(FirstActivity.this
                                    .getApplicationContext());
                    SharedPreferences.Editor mEditor = sharedPreferences.edit();
                    mEditor.putString("token",token);
                    mEditor.putString("transactionID",transactionID);
                    mEditor.commit();

                    Log.i("enc_msg", credBlock.getJSONObject("data").getString("encryptedBase64String"));
                    //Log.i("enc_msg", credBlock.getString("message"));
                     new Loading_fail().execute(TYPE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }



    class Loading_fail extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            requestcheck=true;
            progressBar.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                response = callHTTPService(params[0]);
            }catch(Exception e){
                e.printStackTrace();
                progressBar.show();
                requestcheck = false;
            }

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            try {

                requestcheck = false;
                progressBar.dismiss();
                ll_bottom.setVisibility(View.GONE);
                rl_layout.setVisibility(View.GONE);
                rl_transaction.setVisibility(View.VISIBLE);
                txt_transid.setText("Transaction ID : "+transactionID);

                if (response != null) {
                    if (!response.equalsIgnoreCase("")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String result = jsonObject.getString("result");
                            if (result.equalsIgnoreCase("SUCCESS")) {
                                imgs.setImageResource(R.drawable.done);
                                txt_upperlabel.setText("Transaction Successful");
                                Response(response);
                            } else {
                                imgs.setImageResource(R.drawable.fail);
                                txt_upperlabel.setText("Transaction Failed");
                                Response(response);
                            }

                        }catch (Exception e){

                        }
                    } else {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                } else {
                    setResult(Constants.RESPONSENULL);
                    finish();
                }
            }catch(Exception e){
                progressBar.show();
                requestcheck = false;
            }
        }
    }

    public void Response(final String response){
        try{
            new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    try {
                        Intent intent = new Intent();
                        intent.putExtra("RESPONSE", response);
                        setResult(RESULT_OK, intent);
                        finish();

                    }catch (Exception e){

                    }
                    // close this activity

                }
            }, 3000);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void onBackPressed() {
        if (requestcheck) {
            Toast.makeText(FirstActivity.this, "Please wait.", Toast.LENGTH_LONG).show();

        } else {
            setResult(Constants.BACK);
            finish();
        }
    }
    class C04262 implements X509HostnameVerifier
    {
        C04262() {
    }

    public boolean verify(String s, SSLSession sslSession) {
        return true;
    }

    public void verify(String s, SSLSocket sslSocket) throws IOException {
    }

    public void verify(String s, X509Certificate x509Certificate) throws SSLException {
    }

    public void verify(String s, String[] strings, String[] strings2) throws SSLException {
    }
}


class C02431 implements HostnameVerifier {
    C02431() {
    }

    public boolean verify(String hostname, SSLSession sslSession) {
        return true;
    }
}

    public String callHTTPService(String requesttype) {

        String json = "";
        JSONObject payload = null;
        String URL="";
        try {
            payload = new JSONObject();
            if(requesttype.equalsIgnoreCase("PAY")) {
                URL = "http://103.14.161.149:12001/UpiService/upi/payTransService";
                payload = getParamsforPay();
            }else if(requesttype.equalsIgnoreCase("COLLECT")) {
                URL =  "http://103.14.161.149:12001/UpiService/upi/collectTransService";
                payload = getCollection();
            }else if(requesttype.equalsIgnoreCase("OTP")) {
                URL = "http://103.14.161.149:12001/UpiService/upi/otpService";
                payload = getOTPService();
            }else if(requesttype.equalsIgnoreCase("BALANCE")) {
                URL  ="http://103.14.161.149:12001/UpiService/upi/balanceEnquiry";
                payload = getParamsforBalanceEnquiry();
            }else if(requesttype.equalsIgnoreCase("LISTKEYS")) {
                URL = "http://103.14.161.149:12001/UpiService/upi/listKeysService";
                payload = getParamsListKeys();
            }else if(requesttype.equalsIgnoreCase("MOBILEREGS")) {
                URL = "http://103.14.161.149:12001/UpiService/upi/mobileRegistration";
                payload = getParamsforMobileregistration();
            }else if(requesttype.equalsIgnoreCase("LISTACCOUNT")) {
                URL = "http://103.14.161.149:12001/UpiService/upi/listAccountBankService";
                payload = getParamsforListAccountBank();
            }

            SSLContext sc;
            HttpClient httpClient;
            HttpPost httpPost;
            BufferedReader reader;
            StringBuilder sb;
            String line;
            InputStream is;


            sc = SSLContext.getInstance(SSLSocketFactory.TLS);
            sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new C02431());
            httpClient = new DefaultHttpClient();
            ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04262());
            httpPost = new HttpPost(URL);
            httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
            httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
            httpPost.setHeader("Content-type", "application/json");
            is = httpClient.execute(httpPost).getEntity().getContent();
            reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            sb = new StringBuilder();
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("@GetAccountResponse", json);
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
        } catch (ClientProtocolException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        } catch (NoSuchAlgorithmException e7) {
            e7.printStackTrace();
        } catch (KeyManagementException e8) {
            e8.printStackTrace();
        } catch (Exception e) {
        }

        String response = json.toString();

         return response;
        }


    public JSONObject getParamsforPay(){
        JSONObject payload = null;
        try{
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());

            String token = sharedPreferences.getString("token","1.0|m4yUxkr5G8m/0haq/9m3UfQ1mhjDhhO2TftSeZ1sNeTESK8YVbMNTM+ZPiXy+BvbaIn7YOEej9yMcxcKLcaGrkXSxp90Rt68E/Rb+j4VvarqNABoi+Xg1kAtgGWszKAEaHEnD2xOjHxM8adZG19gC9HyVi4S+x1PZBrWk9JXNQ+iM9kjxvTnZU9wNJfzbebdyTChpZU9K0+kAuLTkt9RkgKGsOXfeOyP9IKfGhd7K6xF9+YkWFqjr73F4F//jqxZxJpOWZUCk81Uwt1/0l/k51ZLAqfmlENyelaDsWAzHYBXBzzapL6yMl972vUc6x5a7W1KcPNG7ZWmQZTD0iXbhw==");
            token=token.replace("\n","");
            token=token.replaceAll("'\'","");
            String transactionID = sharedPreferences.getString("transactionID","5877863");

            payload = new JSONObject();
              payload.put("txnId", transactionID);
           // payload.put("txnId", "11618");
            payload.put("txnNote", "Bill Payment");
            payload.put("payerAddress", payerVpa+"@mapp");
            payload.put("payerName", payerName);
           // payload.put("mobileNumber", "8654589562");
             payload.put("mobileNumber", mobileNumber);
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "103.14.161.149");
            payload.put("type", "mob");
            payload.put("id", "700043");
            //payload.put("id", "799991");
            payload.put("os", "android");
            payload.put("app", "NPCIAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("payerAcAddressType", "ACCOUNT");


            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc",IFSC);
            arrabj.put("acType", "SAVINGS");
           // arrabj.put("acNum", "3411402395");
            arrabj.put("acNum", accNum);
            arrabj.put("iin", "500043");
            arrabj.put("uIdNum", "");
            arrabj.put("mmId", "3043");
            arrabj.put("mobNum", "");
            arrabj.put("cardNum", "");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);

            payload.put("detailsJson", jsonArray);
            payload.put("credType", "PIN");
            payload.put("credSubType", "MPIN");
            payload.put("credDataValue",token);

           // payload.put("credDataValue","1.0|z3WdxfgoQZg9kJWY4Y999HwWHqwHqwt7jyY86Qa5yIjG+sfevCHzeHtd2QDnaQioIeSbPr2RkuHu5TzSO2JsSq9LPcrXUWwhW9C/TKW9oWakGZWFwWc56PKzldBYtiHBr+y70gAlacwxAqtqt+hJmkOg/kGXCrAg91bYjseEmBMtvdC9CFvTRgvReOQJgYklqzMCgEEqyXllcWJFBTci4wMsjDR+TnIJRyd/w5kJmdpTdV2RBsb9gGm1DDtvzXpgClQvA5VNe86MStpZrPA5DkdGpDT4bciKY54kyKM1JinJA+W+bXihVHCfAJjMBk2xycdVbc6LHX2fIw7Fjc0eGA==");
            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");

            payload.put("txnAmmount", transactionAmount);
            payload.put("payeeAddress", payeeVpa+"@mapp");
            payload.put("payeeName", payeeName);
            payload.put("payeeType", "PERSON");
            payload.put("payeeCode", "0000");
            payload.put("IdentityId", "45785454");
            payload.put("custRefId", "FG4563322");
            payload.put("refId", "A45586699633324");
        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getParamsforBalanceEnquiry(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getParamsforMobileregistration(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();

            payload.put("txnId", "5877863");
            payload.put("refId", "5877863");
            payload.put("payerAddress", "nitish@mapp");
            payload.put("payerName", "Nitish Singla");
            payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "288177 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "124.170.23.22");
            payload.put("type", "mob");
            payload.put("id", "750c6be243f1c4b5c9912b95a5742fc5");
            //payload.put("id", "799991");
            payload.put("os", "android");
            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("accountAddressType", "ACCOUNT");
            payload.put("accountIfsc", "MAPP0000103");
            payload.put("accountType", "SAVINGS");
            payload.put("accountNumber", "45151545548799");

            payload.put("regDetailMobile", "9818677385");
            payload.put("regDetailCardDigits", "8477");
            payload.put("regDetailExpDate", "02/24");

            JSONObject arrabj = new JSONObject();
            arrabj.put("type", "PIN");
            arrabj.put("subType", "MPIN");
            arrabj.put("code", "NPCI");
            arrabj.put("ki", "20150822");
            arrabj.put("value","1.0|m4yUxkr5G8m/0haq/9m3UfQ1mhjDhhO2TftSeZ1sNeTESK8YVbMNTM+ZPiXy+BvbaIn7YOEej9yMcxcKLcaGrkXSxp90Rt68E/Rb+j4VvarqNABoi+Xg1kAtgGWszKAEaHEnD2xOjHxM8adZG19gC9HyVi4S+x1PZBrWk9JXNQ+iM9kjxvTnZU9wNJfzbebdyTChpZU9K0+kAuLTkt9RkgKGsOXfeOyP9IKfGhd7K6xF9+YkWFqjr73F4F//jqxZxJpOWZUCk81Uwt1/0l/k51ZLAqfmlENyelaDsWAzHYBXBzzapL6yMl972vUc6x5a7W1KcPNG7ZWmQZTD0iXbhw==");


            // arrabj.put("value","1.0|PL3lyH+Woe09YqioZHZ7UbuUip2IArK2tZ0B2jJth5olgjiyIGODTFBODng+X3WuBpksaTOGeAAtrYFTyXOZL7idHcIb7H5rvCdIZqjKyU/9Zomn1mrEt80C8cBUMK6LgNMc5+O8ly0KWIfQXmlJq3TyU4WQQGz7w92+YULz9Gqo7/166Y8y4//bKE+injrTdx2vIJOpt26aD/H5leYc584T3hzu8O5GUornMFvrrm5MhflsVbkufim7axjGWpX1yBGN6ImzNEKB3b5pVtGPG3nOCvV5A3+kZZJzax3yBFnK6YoORTceEeQGOZD8DVBL+MtRKLRaneJ5QaCuRk/C3A==");

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload.put("credList", jsonArray);

        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getParamsforListAccountBank(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getParamsListKeys(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();
            payload.put("txnId", "15153786");
            payload.put("txnType", "LIST_KEYS");
            payload.put("credType", "CHALLENGE");
            payload.put("credSubType", "INITIAL");
            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");
            payload.put("credDataValue", "1.0|1Mv7Zy+OQ4czvGsHEv7xEiJs69jqagJQPUnfWaPINVCitAHJ3o5Y2snqif97NbK7Dr5pC3UkZiK4wOV6pykzDcT4muwnzpo/ad6op1xyLqSjmM5OH9D/Ok32OO9tDXvdbXO0fFnnZjOFJ7mFbptKDNkOsi9LL4kPNpmRACAqlLSTiW+eSAHwHo/CkwzpXapXDzJ+v2VtDcxPlwDbPCFJs9oHYZaN/+p8DDKVZsin1kiw6eLGEjGEaXvqj6E9wjMOJAo12/is6U0MoK06hd2cSoBddQPgRncj9/34jvZwPieZ5MakdE34OKY70WWPDG1qH6D3hLYug7WYaCaBdS6PtQ==");

        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getOTPService(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }

    public JSONObject getCollection(){
        JSONObject payload = null;
        try{
            payload = new JSONObject();
        }catch(Exception e){
            e.printStackTrace();
        }
        return payload;
    }


    protected void startAnimation(ImageView view){
        RotateAnimation anim = new RotateAnimation(0.0f, 360.0f , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(2000);
        view.setAnimation(anim);
        view.startAnimation(anim);
    }
}
