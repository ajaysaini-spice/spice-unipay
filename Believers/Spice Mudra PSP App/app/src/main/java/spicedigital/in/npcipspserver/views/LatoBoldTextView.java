package spicedigital.in.npcipspserver.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Custom TextView with Roboto TypeFace.
 */
public class LatoBoldTextView extends TextView {

	public LatoBoldTextView(Context context) {
        super(context);
        style(context);
    }

    public LatoBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public LatoBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        try {
			Typeface tf = Typeface.createFromAsset(context.getAssets(),
			        "fonts/Lato-Bold.ttf");
			setTypeface(tf);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
}
