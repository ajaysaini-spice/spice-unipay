package spicedigital.in.npcipspserver.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class LatoRegularButton extends Button {

	public LatoRegularButton(Context context) {
        super(context);
        style(context);
    }

    public LatoRegularButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public LatoRegularButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        try {
			Typeface tf = Typeface.createFromAsset(context.getAssets(),
			        "fonts/Lato-Regular.ttf");
			setTypeface(tf);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
}
