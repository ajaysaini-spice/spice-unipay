package spicedigital.in.npcipspserver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by CH-E01006 on 3/16/2016.
 */
public class CommonUtils {

    public final static String getAppId(Context context) {
        return "123";
    }

    public final static String getMobileNumber() {
        return "9888762945";
    }
    public static String getDeviceId(Context context) {
        String deviceId = "";
        try {
            deviceId = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceId;
    }
    public static String getToken(String challenge,String type) {

        return challenge+type;
    }

    @SuppressLint("NewApi")
    public static void setStatusBarColor(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = ((Activity) context).getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(context.getResources().getColor(
                        R.color.transparentcolor));
            }
        } catch (Exception e) {
        }
    }

}
