package spicedigital.in.npcipspserver;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AUTH;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;

public class MainActivity extends AppCompatActivity {


    MainActivityFragment fragment2;
    private InputStream is;
    private String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



      /*  HTTPClient httpClient = new HTTPClient();
        httpClient.callHTTPService();
        httpClient.Loading_fail.execute();*/
     /* this.fragment2 = new MainActivityFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment, fragment2);
        fragmentTransaction.commit();
*/
   new Loading_fail().execute();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    class C02391 implements HostnameVerifier {
        C02391() {
        }

        public boolean verify(String hostname, SSLSession sslSession) {
            return true;
        }
    }

    class C04262 implements X509HostnameVerifier {
        C04262() {
        }

        public boolean verify(String s, SSLSession sslSession) {
            return true;
        }

        public void verify(String s, SSLSocket sslSocket) throws IOException {
        }

        public void verify(String s, X509Certificate x509Certificate) throws SSLException {
        }

        public void verify(String s, String[] strings, String[] strings2) throws SSLException {
        }
    }


    class C02431 implements HostnameVerifier {
        C02431() {
        }

        public boolean verify(String hostname, SSLSession sslSession) {
            return true;
        }
    }

  // private String URL = "http://103.14.161.149:12001/UpiService/upi/payTransService";
   //private String URL = "http://103.14.161.149:12001/UpiService/upi/listKeysService";

   // private String URL  ="http://103.14.161.149:12001/UpiService/upi/balanceEnquiry";
    private String URL = "http://103.14.161.149:12001/UpiService/upi/mobileRegistration";

   // private String URL="http://103.14.161.149:12001/UpiService/upi/txnConfirmationService";

  // private String URL =  "http://103.14.161.149:12001/UpiService/upi/collectTransService";

    class Loading_fail extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            callHTTPService();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    public String callHTTPService() {


        String json = "";
        JSONObject payload = null;

        try {

            payload = new JSONObject();
            // OTPService
           /* payload.put("txnId", "12332423");
            payload.put("payerAddress", "nikhil@mgat");
            payload.put("payerName", "Nikhil M");
            payload.put("mobileNumber", "8654589562");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Mumbai,Maharashtra ");
            payload.put("ip", "142.12.26.52");
            payload.put("type", "mob");
            payload.put("id", "154fer53dfdf");
            payload.put("os", "android");

            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("accountAddressType", "ACCOUNT");*/

           /* JSONObject payload2 = new JSONObject();
            payload2.put("txnId", String.valueOf("84895995"));
            payload2.put("payerAddress", "aditya@mapp");
            payload2.put("payerName", "vdnvkfl");
            payload2.put("mobileNumber", "9888762945");
            payload2.put("geoCode", "123466454 ");
            payload2.put("location", "Mumbai,Maharashtra ");
            payload2.put("ip", "142.12.26.52");
            payload2.put("type", "mob");
            payload2.put("id", "154fer53dfdf");
            payload2.put("os", "android");
            payload2.put("app", "MAPP_PSP");
            payload2.put("capability", "453453d4f5343434df354");
            payload2.put("accountAddressType", "ACCOUNT");
            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000106");
            arrabj.put("acType", BuildConfig.FLAVOR);
            arrabj.put("acNum", BuildConfig.FLAVOR);
            arrabj.put("iin", BuildConfig.FLAVOR);
            arrabj.put("uIdNum", BuildConfig.FLAVOR);
            arrabj.put("mmId", BuildConfig.FLAVOR);
            arrabj.put("mobNum", BuildConfig.FLAVOR);
            arrabj.put("cardNum", BuildConfig.FLAVOR);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload2.put("detailsJson", jsonArray);
            Log.d("@OTP_Input", payload2.toString());
            payload = payload2;*/

            // List key
           /* payload.put("txnId", "15153786");
            payload.put("txnType", "LIST_KEYS");
            payload.put("credType", "CHALLENGE");
            payload.put("credSubType", "INITIAL");
            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");
            payload.put("credDataValue", "1.0|1Mv7Zy+OQ4czvGsHEv7xEiJs69jqagJQPUnfWaPINVCitAHJ3o5Y2snqif97NbK7Dr5pC3UkZiK4wOV6pykzDcT4muwnzpo/ad6op1xyLqSjmM5OH9D/Ok32OO9tDXvdbXO0fFnnZjOFJ7mFbptKDNkOsi9LL4kPNpmRACAqlLSTiW+eSAHwHo/CkwzpXapXDzJ+v2VtDcxPlwDbPCFJs9oHYZaN/+p8DDKVZsin1kiw6eLGEjGEaXvqj6E9wjMOJAo12/is6U0MoK06hd2cSoBddQPgRncj9/34jvZwPieZ5MakdE34OKY70WWPDG1qH6D3hLYug7WYaCaBdS6PtQ==");
*/

            //payload.put("txnId", "56119411");
           // payload.put("refId", "56119411");

            // Mobile
           // payload.put("txnId", "66369743");
           // payload.put("refId", "66369743");
            payload.put("txnId", "5877863");
            payload.put("refId", "5877863");
            payload.put("payerAddress", "nitish@mapp");
            payload.put("payerName", "Nitish Singla");
            payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "288177 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "124.170.23.22");
            payload.put("type", "mob");
            payload.put("id", "750c6be243f1c4b5c9912b95a5742fc5");
            //payload.put("id", "799991");
            payload.put("os", "android");
            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("accountAddressType", "ACCOUNT");
            payload.put("accountIfsc", "MAPP0000103");
            payload.put("accountType", "SAVINGS");
            payload.put("accountNumber", "45151545548799");

            payload.put("regDetailMobile", "9818677385");
            payload.put("regDetailCardDigits", "8477");
           payload.put("regDetailExpDate", "02/24");

            JSONObject arrabj = new JSONObject();
           arrabj.put("type", "PIN");
            arrabj.put("subType", "MPIN");
            arrabj.put("code", "NPCI");
            arrabj.put("ki", "20150822");
            arrabj.put("value","1.0|m4yUxkr5G8m/0haq/9m3UfQ1mhjDhhO2TftSeZ1sNeTESK8YVbMNTM+ZPiXy+BvbaIn7YOEej9yMcxcKLcaGrkXSxp90Rt68E/Rb+j4VvarqNABoi+Xg1kAtgGWszKAEaHEnD2xOjHxM8adZG19gC9HyVi4S+x1PZBrWk9JXNQ+iM9kjxvTnZU9wNJfzbebdyTChpZU9K0+kAuLTkt9RkgKGsOXfeOyP9IKfGhd7K6xF9+YkWFqjr73F4F//jqxZxJpOWZUCk81Uwt1/0l/k51ZLAqfmlENyelaDsWAzHYBXBzzapL6yMl972vUc6x5a7W1KcPNG7ZWmQZTD0iXbhw==");


           // arrabj.put("value","1.0|PL3lyH+Woe09YqioZHZ7UbuUip2IArK2tZ0B2jJth5olgjiyIGODTFBODng+X3WuBpksaTOGeAAtrYFTyXOZL7idHcIb7H5rvCdIZqjKyU/9Zomn1mrEt80C8cBUMK6LgNMc5+O8ly0KWIfQXmlJq3TyU4WQQGz7w92+YULz9Gqo7/166Y8y4//bKE+injrTdx2vIJOpt26aD/H5leYc584T3hzu8O5GUornMFvrrm5MhflsVbkufim7axjGWpX1yBGN6ImzNEKB3b5pVtGPG3nOCvV5A3+kZZJzax3yBFnK6YoORTceEeQGOZD8DVBL+MtRKLRaneJ5QaCuRk/C3A==");

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload.put("credList", jsonArray);


           /* payload.put("credDataValue", "1.0|K8zWQGH/eJYHAVwW6gu7e2XXIgeWvUG6KQQ5HxWCt/mq9eSfMtJCsIRpn0g0I1l5EJE2OrFhfacNI9zN7jgSSOElKFNwUeWwt1iKssVLxXWSXtNkMZYsXBQXziyacZJ5azxV6R54ShFBeGmT/5Gku0bHPbU7kPwSCF9zd3ffehFx95XCXgqNfrUHePyBVjWBhqVR5mQZVUQS+ED6beB2KENUlatOZlALcBRtqTpE9kZ8WYhr4Ejk9ojdmgjJuZpYr4ELwlZFi+HgZKZSNr9ayaCCafgHijgByvIob+gU4FAv8ef8JPfv0bVyDJi1eUzW50OQ2EFKeg8npynmx+0Jg==");
*/


           // balanace enquiry
          /*  payload.put("txnId", "15153786");
            payload.put("txnNote", "Bill Payment");
            payload.put("payerAddress", "zeeshan.khan@mapp");

            payload.put("payerName", "Zeeshan khan");
            //  payload.put("mobileNumber", "8654589562");
            payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "142.12.26.52");
            payload.put("type", "mob");
            payload.put("id", "154fer53dfdf");
            //payload.put("id", "799991");
            payload.put("os", "android");

            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("payerAcAddressType", "ACCOUNT");

            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000103");
            arrabj.put("acType", "SAVINGS");
            arrabj.put("acNum", "45151545548799");
            arrabj.put("iin", "");
            arrabj.put("uIdNum", "");
            arrabj.put("mmId", "");
            arrabj.put("mobNum", "");
            arrabj.put("cardNum", "");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload.put("detailsJson", jsonArray);

            payload.put("credType", "PIN");
            payload.put("credSubType", "MPIN");
            payload.put("credDataValue", "1.0|1Mv7Zy+OQ4czvGsHEv7xEiJs69jqagJQPUnfWaPINVCitAHJ3o5Y2snqif97NbK7Dr5pC3UkZiK4wOV6pykzDcT4muwnzpo/ad6op1xyLqSjmM5OH9D/Ok32OO9tDXvdbXO0fFnnZjOFJ7mFbptKDNkOsi9LL4kPNpmRACAqlLSTiW+eSAHwHo/CkwzpXapXDzJ+v2VtDcxPlwDbPCFJs9oHYZaN/+p8DDKVZsin1kiw6eLGEjGEaXvqj6E9wjMOJAo12/is6U0MoK06hd2cSoBddQPgRncj9/34jvZwPieZ5MakdE34OKY70WWPDG1qH6D3hLYug7WYaCaBdS6PtQ==");
*/

            //  payload.put("credDataCode", "NPCI");
          //  payload.put("credDataKi", "20150822");



         // collect
          //  payload.put("txnId", "56119411");
          /*  payload.put("txnId", "66369743");

            payload.put("txnNote", "Bill Payment");
            payload.put("payerAddress", "zeeshan.khan@mapp");
            payload.put("payerName", "Zeeshan khan");
            payload.put("mobileNumber", "8654589562");
          //  payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "142.12.26.52");
            payload.put("type", "mob");
            payload.put("id", "154fer53dfdf");
            //payload.put("id", "799991");
            payload.put("os", "android");

            payload.put("app", "MGSAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("payerAcAddressType", "ACCOUNT");
            payload.put("txnAmmount", "200");
            payload.put("payeeAddress", "rohit.patkar@mapp");
            payload.put("payeeName", "Aditya Bera");
            payload.put("payeeType", "PERSON");
            payload.put("payeeCode", "0000");
            payload.put("IdentityId", "45785454");
            payload.put("refId", "66369743");
            payload.put("custRefId", "18622335");

            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000103");
            arrabj.put("acType", "SAVINGS");
            arrabj.put("acNum", "45151545548799");
            arrabj.put("iin", "");
            arrabj.put("uIdNum", "");
            arrabj.put("mmId", "");
            arrabj.put("mobNum", "");
            arrabj.put("cardNum", "");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);
            payload.put("detailsJson", jsonArray);
            payload.put("credType", "PIN");
            payload.put("credSubType", "MPIN");

           *//* payload.put("credDataValue", "1.0|K8zWQGH/eJYHAVwW6gu7e2XXIgeWvUG6KQQ5HxWCt/mq9eSfMtJCsIRpn0g0I1l5EJE2OrFhfacNI9zN7jgSSOElKFNwUeWwt1iKssVLxXWSXtNkMZYsXBQXziyacZJ5azxV6R54ShFBeGmT/5Gku0bHPbU7kPwSCF9zd3ffehFx95XCXgqNfrUHePyBVjWBhqVR5mQZVUQS+ED6beB2KENUlatOZlALcBRtqTpE9kZ8WYhr4Ejk9ojdmgjJuZpYr4ELwlZFi+HgZKZSNr9ayaCCafgHijgByvIob+gU4FAv8ef8JPfv0bVyDJi1eUzW50OQ2EFKeg8npynmx+0Jg==");
*//*

          *//*  payload.put("credDataValue", "1.0|1Mv7Zy+OQ4czvGsHEv7xEiJs69jqagJQPUnfWaPINVCitAHJ3o5Y2snqif97NbK7Dr5pC3UkZiK4wOV6pykzDcT4muwnzpo/ad6op1xyLqSjmM5OH9D/Ok32OO9tDXvdbXO0fFnnZjOFJ7mFbptKDNkOsi9LL4kPNpmRACAqlLSTiW+eSAHwHo/CkwzpXapXDzJ+v2VtDcxPlwDbPCFJs9oHYZaN/+p8DDKVZsin1kiw6eLGEjGEaXvqj6E9wjMOJAo12/is6U0MoK06hd2cSoBddQPgRncj9/34jvZwPieZ5MakdE34OKY70WWPDG1qH6D3hLYug7WYaCaBdS6PtQ==");
*//*

            *//*payload.put("credDataValue","1.0|0jRAVrn6tCwP5MO1vwiyeLliKZmGfgJwhb0a4wcPV8DVyRmO6glK5C59r/n4J7ZxCNgjjKkZYGN72DPy/UbNkK84vYlbCixXJ8nPERLZSezJIrDX1575PcZbBVs2EBXCTNh0CUt1B9j4Wrpz7DwN30hnu132nN5f3VqVnkGK6LFedwZUJNSaeL5CCnVka9qjfxLMfE/Y6Xs13mYlEAamB4PRmGcngj5d2+IdPxcH/FNr0nJoF7yuAJQRVVyTX3g4aQt3Z4Fc5DcC3J6PYLVko+isNPbwv5K9WkihI4xV73F4FVE7VFg5sHorMkD+T9t+PWNrbZ6fS/JlHCUWlDVInA==");
*//*

            arrabj.put("credDataValue","1.0|PL3lyH+Woe09YqioZHZ7UbuUip2IArK2tZ0B2jJth5olgjiyIGODTFBODng+X3WuBpksaTOGeAAtrYFTyXOZL7idHcIb7H5rvCdIZqjKyU/9Zomn1mrEt80C8cBUMK6LgNMc5+O8ly0KWIfQXmlJq3TyU4WQQGz7w92+YULz9Gqo7/166Y8y4//bKE+injrTdx2vIJOpt26aD/H5leYc584T3hzu8O5GUornMFvrrm5MhflsVbkufim7axjGWpX1yBGN6ImzNEKB3b5pVtGPG3nOCvV5A3+kZZJzax3yBFnK6YoORTceEeQGOZD8DVBL+MtRKLRaneJ5QaCuRk/C3A==");


            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");*/


            //payrequest
         /*   payload.put("txnId", "5877863");
           // payload.put("txnId", "11618");
            payload.put("txnNote", "Bill Payment");
            payload.put("payerAddress", "rohit.patkar@mapp");
            payload.put("payerName", "Aditya Bera");
           // payload.put("mobileNumber", "8654589562");
             payload.put("mobileNumber", "9818677385");
            payload.put("geoCode", "123466454 ");
            payload.put("location", "Maharashtra");
            payload.put("ip", "103.14.161.149");
            payload.put("type", "mob");
            payload.put("id", "700043");
            //payload.put("id", "799991");
            payload.put("os", "android");
            payload.put("app", "NPCIAPP");
            payload.put("capability", "453453d4f5343434df354");
            payload.put("payerAcAddressType", "ACCOUNT");


            JSONObject arrabj = new JSONObject();
            arrabj.put("ifsc", "MAPP0000103");
            arrabj.put("acType", "SAVINGS");
            arrabj.put("acNum", "3411402395");
            arrabj.put("iin", "500043");
            arrabj.put("uIdNum", "");
            arrabj.put("mmId", "3043");
            arrabj.put("mobNum", "");
            arrabj.put("cardNum", "");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(arrabj);

            payload.put("detailsJson", jsonArray);
            payload.put("credType", "PIN");
            payload.put("credSubType", "MPIN");
            payload.put("credDataValue","1.0|m4yUxkr5G8m/0haq/9m3UfQ1mhjDhhO2TftSeZ1sNeTESK8YVbMNTM+ZPiXy+BvbaIn7YOEej9yMcxcKLcaGrkXSxp90Rt68E/Rb+j4VvarqNABoi+Xg1kAtgGWszKAEaHEnD2xOjHxM8adZG19gC9HyVi4S+x1PZBrWk9JXNQ+iM9kjxvTnZU9wNJfzbebdyTChpZU9K0+kAuLTkt9RkgKGsOXfeOyP9IKfGhd7K6xF9+YkWFqjr73F4F//jqxZxJpOWZUCk81Uwt1/0l/k51ZLAqfmlENyelaDsWAzHYBXBzzapL6yMl972vUc6x5a7W1KcPNG7ZWmQZTD0iXbhw==");


           // payload.put("credDataValue","1.0|z3WdxfgoQZg9kJWY4Y999HwWHqwHqwt7jyY86Qa5yIjG+sfevCHzeHtd2QDnaQioIeSbPr2RkuHu5TzSO2JsSq9LPcrXUWwhW9C/TKW9oWakGZWFwWc56PKzldBYtiHBr+y70gAlacwxAqtqt+hJmkOg/kGXCrAg91bYjseEmBMtvdC9CFvTRgvReOQJgYklqzMCgEEqyXllcWJFBTci4wMsjDR+TnIJRyd/w5kJmdpTdV2RBsb9gGm1DDtvzXpgClQvA5VNe86MStpZrPA5DkdGpDT4bciKY54kyKM1JinJA+W+bXihVHCfAJjMBk2xycdVbc6LHX2fIw7Fjc0eGA==");
            payload.put("credDataCode", "NPCI");
            payload.put("credDataKi", "20150822");

            payload.put("txnAmmount", "200");
            payload.put("payeeAddress", "zeeshan.khan@mapp");
            payload.put("payeeName", "Zeeshan khan");
            payload.put("payeeType", "PERSON");
            payload.put("payeeCode", "0000");
            payload.put("IdentityId", "45785454");
            payload.put("custRefId", "FG4563322");
            payload.put("refId", "A45586699633324");*/



           /* SSLContext sc = null;
            try {
                sc = SSLContext.getInstance(SSLSocketFactory.TLS);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            try {
                sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new C02391());*/



            SSLContext sc;
            HttpClient httpClient;
            HttpPost httpPost;
            BufferedReader reader;
            StringBuilder sb;
            String line;
            InputStream is;


            sc = SSLContext.getInstance(SSLSocketFactory.TLS);
            sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new C02431());
            httpClient = new DefaultHttpClient();
            ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04262());
            httpPost = new HttpPost(URL);
            httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
            httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
            httpPost.setHeader("Content-type", "application/json");
            is = httpClient.execute(httpPost).getEntity().getContent();
            reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            sb = new StringBuilder();
            while (true) {
                line = reader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("@GetAccountResponse", json);
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
        } catch (ClientProtocolException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        } catch (NoSuchAlgorithmException e7) {
            e7.printStackTrace();
        } catch (KeyManagementException e8) {
            e8.printStackTrace();
        } catch (JSONException e) {
        }


          /*  HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL);
            httpPost.setEntity((new StringEntity(payload.toString(), "UTF-8")));
            httpPost.setHeader("Authorization",
                    "Basic bWdzdXBpOmFkbWluQDEyMw=="); // +
            // LoginActivity.token.getToken());
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            Log.e("@Response", json);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

        String response = json.toString();

        return response;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class GetresAsync extends AsyncTask<DetailDTO, Void, String> {
        private ProgressDialog nDialog;

        /* renamed from: com.mindgate.psp.Fragment.Fragment2.GetresAsync.1 */
        class C02411 implements HostnameVerifier {
            C02411() {
            }

            public boolean verify(String hostname, SSLSession sslSession) {
                return true;
            }
        }

        /* renamed from: com.mindgate.psp.Fragment.Fragment2.GetresAsync.2 */
        class C04252 implements X509HostnameVerifier {
            C04252() {
            }

            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }

            public void verify(String s, SSLSocket sslSocket) throws IOException {
            }

            public void verify(String s, X509Certificate x509Certificate) throws SSLException {
            }

            public void verify(String s, String[] strings, String[] strings2) throws SSLException {
            }
        }

        protected void onPreExecute() {
            super.onPreExecute();
            this.nDialog = new ProgressDialog(MainActivity.this);
            this.nDialog.setMessage("Loading..");
            this.nDialog.setIndeterminate(false);
            this.nDialog.setCancelable(false);
            this.nDialog.show();
        }

        protected String doInBackground(DetailDTO... detailDTO) {
            JSONException e;
            SSLContext sc=null;
            HttpClient httpClient;
            HttpPost httpPost;
            BufferedReader reader=null;
            StringBuilder sb;
            String line=null;
            JSONObject payload = null;
            try {
                JSONObject payload2 = new JSONObject();
                try {
                    payload2.put("txnId", String.valueOf(detailDTO[0].getTxnId()));
                    payload2.put("payerAddress", detailDTO[0].getPayeeaddr());
                    payload2.put("payerName", detailDTO[0].getPayeename());
                    payload2.put("mobileNumber", detailDTO[0].getMobno());
                    payload2.put("geoCode", "123466454 ");
                    payload2.put("location", "Mumbai,Maharashtra ");
                    payload2.put("ip", "142.12.26.52");
                    payload2.put("type", "mob");
                    payload2.put("id", "154fer53dfdf");
                    payload2.put("os", "android");
                    payload2.put("app", "MAPP_PSP");
                    payload2.put("capability", "453453d4f5343434df354");
                    payload2.put("accountAddressType", "ACCOUNT");
                    JSONObject arrabj = new JSONObject();
                    arrabj.put("ifsc", "MAPP0000106");
                    arrabj.put("acType", BuildConfig.FLAVOR);
                    arrabj.put("acNum", BuildConfig.FLAVOR);
                    arrabj.put("iin", BuildConfig.FLAVOR);
                    arrabj.put("uIdNum", BuildConfig.FLAVOR);
                    arrabj.put("mmId", BuildConfig.FLAVOR);
                    arrabj.put("mobNum", BuildConfig.FLAVOR);
                    arrabj.put("cardNum", BuildConfig.FLAVOR);
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(arrabj);
                    payload2.put("detailsJson", jsonArray);
                    Log.d("@OTP_Input", payload2.toString());
                    payload = payload2;
                } catch (JSONException e2) {
                    e = e2;
                    payload = payload2;
                    e.printStackTrace();
                    try {
                        sc = SSLContext.getInstance(SSLSocketFactory.TLS);
                    } catch (NoSuchAlgorithmException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
                    } catch (KeyManagementException e1) {
                        e1.printStackTrace();
                    }
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    HttpsURLConnection.setDefaultHostnameVerifier(new C02411());
                    httpClient = new DefaultHttpClient();
                    ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04252());
                    httpPost = new HttpPost(URL);
                    try {
                        httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                    httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
                    httpPost.setHeader("Content-type", "application/json");
                    try {
                        is = httpClient.execute(httpPost).getEntity().getContent();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    try {
                        reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }
                    sb = new StringBuilder();
                    /*while (true) {
                        try {
                            line = reader.readLine();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        if (line == null) {
                            break;
                            try {
                                is.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            json = sb.toString();
                            Log.e("@Response", json);
                            return json;
                        }
                        sb.append(line + "\n");
                    }*/
                }
            } catch (Exception e3) {
               // e = e3;

                try {
                    sc = SSLContext.getInstance(SSLSocketFactory.TLS);
                } catch (NoSuchAlgorithmException e1) {
                    e1.printStackTrace();
                }
                try {
                    sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
                } catch (KeyManagementException e1) {
                    e1.printStackTrace();
                }
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new C02411());
                httpClient = new DefaultHttpClient();
                ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04252());
                httpPost = new HttpPost(URL);
                try {
                    httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
                httpPost.setHeader("Content-type", "application/json");
                try {
                    is = httpClient.execute(httpPost).getEntity().getContent();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
                sb = new StringBuilder();
                /*while (true) {
                    try {
                        line = reader.readLine();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    if (line == null) {
                        sb.append(line + "\n");
                    } else {
                        break;
                        try {
                            is.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        json = sb.toString();
                        Log.e("@Response", json);
                        return json.toString();
                    }
                }*/
            }
            try {
                sc = SSLContext.getInstance(SSLSocketFactory.TLS);
                sc.init(null, new TrustManager[]{new TrustAllX509TrustManager()}, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new C02411());
                httpClient = new DefaultHttpClient();
                ((SSLSocketFactory) httpClient.getConnectionManager().getSchemeRegistry().getScheme("https").getSocketFactory()).setHostnameVerifier(new C04252());
                httpPost = new HttpPost(URL);
                httpPost.setEntity(new StringEntity(payload.toString(), HTTP.UTF_8));
                httpPost.setHeader(AUTH.WWW_AUTH_RESP, "Basic bWdzdXBpOmFkbWluQDEyMw==");
                httpPost.setHeader("Content-type", "application/json");
                is = httpClient.execute(httpPost).getEntity().getContent();
                reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                sb = new StringBuilder();
                while (true) {
                    line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(line + "\n");
                }
               is.close();
                json = sb.toString();
                Log.e("@Response", json);
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
            } catch (ClientProtocolException e5) {
                e5.printStackTrace();
            } catch (IOException e6) {
                e6.printStackTrace();
            } catch (NoSuchAlgorithmException e7) {
                e7.printStackTrace();
            } catch (KeyManagementException e8) {
                e8.printStackTrace();
            }
            return json.toString();
        }

        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            this.nDialog.dismiss();
            try {
                if (!new JSONObject(res).getString("result").toUpperCase().equals("SUCCESS")) {
                    return;
                }
               /* if (ConnectionDetector.isNetworkReady(Fragment2.this.getActivity())) {
                    new GET_TRAN_ACCOUNTLIST(null).execute(new String[0]);
                } else {
                    ConnectionDetector.showAlertMsg(Fragment2.this.getActivity(), "Network is not connected. Please try again", "Error");
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("@Except", BuildConfig.FLAVOR);
            }
        }
    }
    private void sendPostRequest(String ownerCode) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String ownerCode = params[0];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost("http://172.18.17.51:9004/upi/requestListKeys");

                try {

                    String xmlRequestString="<upi:ReqListKeys xmlns:upi=\"http://npci.org/upi/schema/1/0/\"> \n" +
                            "<Head ver=\"1.0\" ts=\"2015-01-16T14:15:43+05:30\" orgId=\""+ownerCode+"\" msgId=\"1\"/>\n" +
                            "</upi:ReqListKeys>";


                    StringEntity se = new StringEntity( xmlRequestString, HTTP.UTF_8);
                    se.setContentType("text/xml");
                    httpPost.setEntity(se);

                    HttpResponse httpResponse = httpClient.execute(httpPost);

                    InputStream inputStream = httpResponse.getEntity().getContent();

                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();

                    String bufferedStrChunk = null;

                    while((bufferedStrChunk = bufferedReader.readLine()) != null){
                        stringBuilder.append(bufferedStrChunk);
                    }

                    return stringBuilder.toString();

                } catch (ClientProtocolException cpe) {
                    System.out.println("Protocol HttpResponese :" + cpe);
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    System.out.println("IO Exception HttpResponse :" + ioe);
                    ioe.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                String xmlPayloadString=result;
                Toast.makeText(getApplicationContext(), "ListKeys XML Loaded Successfully...", Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(ownerCode);
    }
}
